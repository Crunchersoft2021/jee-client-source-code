package ui;

import JEE.graphs.BarChartDemo1;
import JEE.graphs.GraphingData11;
import JEE.pdfViewer.pdfopener;
import java.awt.*;
import java.util.*;
import JEE.test.TestBean;
import JEE.test.DBConnection;
import JEE.practice.*;
import javax.swing.*;
import JEE.test.SaveAllTests;
import JEE.unitTest.UnitTestBean;
import java.awt.event.InputEvent;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.ServerMonitorSocket;


public class NewTestForm extends javax.swing.JFrame {

    static int n1, m1;
    int unittest = 0;
    JButton strt = new JButton();
    JButton back = new JButton();
    JButton back1 = new JButton();
    JPanel jp1 = new JPanel();
    JEE.unitTest.DBConnection db = new JEE.unitTest.DBConnection();
    JPanel jp = new JPanel();
    boolean flagGroup = true, flagSubject = true, flagChapter = true;
    LinkedHashMap<String, Integer> lh;
    DBConnection con;
    private javax.swing.JCheckBox[] jCheckboxArray;
    private javax.swing.JButton[] jButtonsArray;
    int p, c, b, total, subjectId, chapterId, subjectId1, chapterId1, totalTime, CheckBoxNumber, ButtonsNo, groupId, gg = 0, rollNo;
    ArrayList<String> nameofchapters, name;
    PracticeBean practiceBean;
    SaveAllTests saveAllTest = new SaveAllTests();
    
    
    

    public NewTestForm(int rollNo) {
        this.rollNo = rollNo;
        initComponents();
        try {
            new ServerMonitorSocket();
        } catch (IOException ex) {
            Logger.getLogger(NewTestForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.getContentPane().setBackground(Color.white);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String FULLNAME = db.getNameOfStudent(rollNo);
        
        System.out.println("FULLNAME------------"+FULLNAME);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        con = new DBConnection();
        p = c = b = total = 0;
        strt.setFont(new java.awt.Font("Times New Roman", 1, 22));
        back.setFont(new java.awt.Font("Times New Roman", 1, 22));
        int m = saveAllTest.isTestPresent(rollNo);
        if (m == 1) {
            System.out.println("1---------------present"); 
        } else {
            System.out.println("0---------------not present");
        }
        
//        jLabel1.setText("<html><tr><td>"
//                + "<img src=\""
//                + NewTestForm.class.getResource("/ui/images/shahu-logo.gif")
//                + "\"></td>"
//                + "<td><center><FONT color=\"#000000\",size=\"5\">Shiv Chhatrapati Shikshan Sanstha's</FONT><br /><HR><FONT color=\"#ED5E10\",size=\"6\"><b>RAJARSHI SHAHU MAHAVIDYALAYA (AUTONOMOUS), LATUR</b></FONT><HR><FONT color=\"#000000\",size=\"5\">NAAC Reaccredited \"A\" Grade with 3.38 CGPA Score and CPE Status</FONT></td></tr><html>");

                jLabel1.setText("<html><tr><td>"
                + "<center><img src=\""
                + NewTestForm.class.getResource("/ui/images/c.gif")
                + "\"></center></td>"
                + "<td><center><FONT color=\"#000000\",size=\"5\"></FONT><br /><HR><FONT color=\"#ED5E10\",size=\"6\"><b>Client Side JEE</b></FONT><HR><FONT color=\"#000000\",size=\"5\"></FONT></td></tr><html>");
    
//                lblInstructions.setVisible(false);
 }

    private void btnStrtActionPerformed(java.awt.event.ActionEvent evt, int unittestsubid) {
        nameofchapters = new ArrayList<String>();
        int i = 0;
        for (int x = 0; x < CheckBoxNumber; x++) {
            if (jCheckboxArray[x].isSelected()) {
                nameofchapters.add(name.get(x));
                String str = nameofchapters.get(i);
                i++;
                System.out.println(str);
            }
        }
        if (nameofchapters.size() > 1) {
            JEE.unitTest.DBConnection conn = new JEE.unitTest.DBConnection();
            ArrayList<Integer> chapids = conn.getChapIds(nameofchapters);
            UnitTestBean unitTestBean = conn.getUnitTestBean(unittestsubid, chapids);
            if (unitTestBean != null) {
                StartedUnitTestForm t = new StartedUnitTestForm(unitTestBean, true, rollNo);
                t.setVisible(true);
                this.dispose();
            }
        } else {
            JOptionPane.showMessageDialog(null, "At least 2 chapters are required.");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        HederPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        MiddleHeaderPanel = new javax.swing.JPanel();
        lblTitle = new javax.swing.JLabel();
        BodyPanel = new javax.swing.JPanel();
        lblStartTest = new javax.swing.JLabel();
        lblInstructions = new javax.swing.JLabel();
        lblAboutUs = new javax.swing.JLabel();
        lblSignOut = new javax.swing.JLabel();
        lblTestReport = new javax.swing.JLabel();
        lblStudentProfile = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("CruncherSoft Technologies JEE Client Application"); // NOI18N
        setBackground(new java.awt.Color(255, 255, 255));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        HederPanel.setBackground(new java.awt.Color(255, 255, 255));
        HederPanel.setName("HederPanel"); // NOI18N

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setName("jLabel1"); // NOI18N

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(29, 9, 44));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setName("jLabel6"); // NOI18N

        javax.swing.GroupLayout HederPanelLayout = new javax.swing.GroupLayout(HederPanel);
        HederPanel.setLayout(HederPanelLayout);
        HederPanelLayout.setHorizontalGroup(
            HederPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(HederPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(HederPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(HederPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        HederPanelLayout.setVerticalGroup(
            HederPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, HederPanelLayout.createSequentialGroup()
                .addComponent(jLabel6)
                .addGap(5, 5, 5)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        MiddleHeaderPanel.setBackground(new java.awt.Color(58, 18, 88));
        MiddleHeaderPanel.setName("MiddleHeaderPanel"); // NOI18N

        lblTitle.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        lblTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitle.setText("CruncherSoft Technologies Client-Server Application");
        lblTitle.setToolTipText("");
        lblTitle.setName("lblTitle"); // NOI18N

        javax.swing.GroupLayout MiddleHeaderPanelLayout = new javax.swing.GroupLayout(MiddleHeaderPanel);
        MiddleHeaderPanel.setLayout(MiddleHeaderPanelLayout);
        MiddleHeaderPanelLayout.setHorizontalGroup(
            MiddleHeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, MiddleHeaderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        MiddleHeaderPanelLayout.setVerticalGroup(
            MiddleHeaderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MiddleHeaderPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitle)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        BodyPanel.setBackground(new java.awt.Color(29, 9, 44));
        BodyPanel.setName("BodyPanel"); // NOI18N

        lblStartTest.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblStartTest.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/Start_Test.png"))); // NOI18N
        lblStartTest.setName("lblStartTest"); // NOI18N
        lblStartTest.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                lblStartTestMouseMoved(evt);
            }
        });
        lblStartTest.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblStartTestMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblStartTestMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblStartTestMouseExited(evt);
            }
        });

        lblInstructions.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblInstructions.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/Setting.png"))); // NOI18N
        lblInstructions.setName("lblInstructions"); // NOI18N
        lblInstructions.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblInstructionsMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblInstructionsMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblInstructionsMouseExited(evt);
            }
        });

        lblAboutUs.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAboutUs.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/About_Us.png"))); // NOI18N
        lblAboutUs.setName("lblAboutUs"); // NOI18N
        lblAboutUs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblAboutUsMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblAboutUsMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblAboutUsMouseExited(evt);
            }
        });

        lblSignOut.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSignOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/Sign_Out.png"))); // NOI18N
        lblSignOut.setName("lblSignOut"); // NOI18N
        lblSignOut.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblSignOutMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblSignOutMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblSignOutMouseExited(evt);
            }
        });

        lblTestReport.setBackground(new java.awt.Color(255, 153, 0));
        lblTestReport.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lblTestReport.setForeground(new java.awt.Color(255, 255, 255));
        lblTestReport.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTestReport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/TestReport.png"))); // NOI18N
        lblTestReport.setName("lblTestReport"); // NOI18N
        lblTestReport.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblTestReportMouseClicked(evt);
            }
        });

        lblStudentProfile.setForeground(new java.awt.Color(255, 255, 255));
        lblStudentProfile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/Student_Profile.png"))); // NOI18N
        lblStudentProfile.setText("jLabel2");
        lblStudentProfile.setName("lblStudentProfile"); // NOI18N
        lblStudentProfile.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblStudentProfileMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout BodyPanelLayout = new javax.swing.GroupLayout(BodyPanel);
        BodyPanel.setLayout(BodyPanelLayout);
        BodyPanelLayout.setHorizontalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblStartTest)
                .addGap(19, 19, 19)
                .addComponent(lblStudentProfile, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblTestReport)
                .addGap(27, 27, 27)
                .addComponent(lblInstructions)
                .addGap(19, 19, 19)
                .addComponent(lblAboutUs)
                .addGap(18, 18, 18)
                .addComponent(lblSignOut)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        BodyPanelLayout.setVerticalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyPanelLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblInstructions)
                    .addComponent(lblStartTest)
                    .addComponent(lblAboutUs)
                    .addComponent(lblSignOut)
                    .addComponent(lblTestReport)
                    .addComponent(lblStudentProfile))
                .addContainerGap(1030, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(HederPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(MiddleHeaderPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(BodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(HederPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(MiddleHeaderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(BodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static int rn1() {
        return n1;
    }

    public static int rm1() {
        return m1;
    }

    public void logout(int roll) {
        try {
            int r = roll;
            JEE.test.DBConnection db = new JEE.test.DBConnection();
            int l = db.logout(r);
            if (l == 1) {
                this.dispose();
                new LoginStudentForm().setVisible(true);                
               // System.exit(0); 
            } else {
                JOptionPane.showMessageDialog(null, "Cannot Logout, Try Again...!");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NewTestForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Are You Sure to Logout?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            logout(rollNo);
        }
    }//GEN-LAST:event_formWindowClosing

    private void lblStartTestMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblStartTestMouseClicked
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                
                new ResumeClassTests(rollNo).setVisible(true);
                //this.dispose();
            }
        }
    }//GEN-LAST:event_lblStartTestMouseClicked

    private void lblSignOutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSignOutMouseClicked
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
                Object[] options = {"YES", "CANCEL"};
                int i = JOptionPane.showOptionDialog(null, "Are You Sure to Logout?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                if (i == 0) {
                    logout(rollNo);
                }
            }
        }
    }//GEN-LAST:event_lblSignOutMouseClicked

    private void lblStartTestMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblStartTestMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_lblStartTestMouseMoved

    private void lblStartTestMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblStartTestMouseEntered
        lblStartTest.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/Start_Test1.png"))); // NOI18N
    }//GEN-LAST:event_lblStartTestMouseEntered

    private void lblStartTestMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblStartTestMouseExited
        lblStartTest.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/Start_Test.png"))); // NOI18N
    }//GEN-LAST:event_lblStartTestMouseExited

    private void lblInstructionsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblInstructionsMouseEntered
//        lblInstructions.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/Setting.png"))); // NOI18N
    }//GEN-LAST:event_lblInstructionsMouseEntered

    private void lblInstructionsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblInstructionsMouseExited
//        lblInstructions.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/Setting.png"))); // NOI18N
    }//GEN-LAST:event_lblInstructionsMouseExited

    private void lblAboutUsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAboutUsMouseEntered
        lblAboutUs.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/About_Us1.png"))); // NOI18N
    }//GEN-LAST:event_lblAboutUsMouseEntered

    private void lblAboutUsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAboutUsMouseExited
        lblAboutUs.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/About_Us.png"))); // NOI18N
    }//GEN-LAST:event_lblAboutUsMouseExited

    private void lblSignOutMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSignOutMouseEntered
        lblSignOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/Sign_Out1.png"))); // NOI18N
    }//GEN-LAST:event_lblSignOutMouseEntered

    private void lblSignOutMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSignOutMouseExited
        lblSignOut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/HomePage/Sign_Out.png"))); // NOI18N
    }//GEN-LAST:event_lblSignOutMouseExited

    private void lblInstructionsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblInstructionsMouseClicked
//        switch (evt.getModifiers()) {
//            case InputEvent.BUTTON1_MASK: {
//                //Code
//                
//            }
//        }
      new OSKSetting().setVisible(true);
    }//GEN-LAST:event_lblInstructionsMouseClicked

    private void lblAboutUsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAboutUsMouseClicked
        switch (evt.getModifiers()) {
            case InputEvent.BUTTON1_MASK: {
            try {
                new AboutUs(rollNo,this).setVisible(true);
            } catch (URISyntaxException ex) {
                Logger.getLogger(NewTestForm.class.getName()).log(Level.SEVERE, null, ex);
            }
                this.dispose();
            }
        }
    }//GEN-LAST:event_lblAboutUsMouseClicked

    private void lblTestReportMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTestReportMouseClicked
        // TODO add your handling code here:
        new TestReport(rollNo).setVisible(true);
    }//GEN-LAST:event_lblTestReportMouseClicked

    private void lblStudentProfileMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblStudentProfileMouseClicked
        // TODO add your handling code here:
        new StudentProfile(rollNo).setVisible(true);
    }//GEN-LAST:event_lblStudentProfileMouseClicked

    public static void main(String args[]) {
        new NewTestForm(1).setVisible(true);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BodyPanel;
    private javax.swing.JPanel HederPanel;
    private javax.swing.JPanel MiddleHeaderPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel lblAboutUs;
    private javax.swing.JLabel lblInstructions;
    private javax.swing.JLabel lblSignOut;
    private javax.swing.JLabel lblStartTest;
    private javax.swing.JLabel lblStudentProfile;
    private javax.swing.JLabel lblTestReport;
    private javax.swing.JLabel lblTitle;
    // End of variables declaration//GEN-END:variables
}