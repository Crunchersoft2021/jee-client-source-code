package ui;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.Timer;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import JEE.practice.PracticeBean;

import JEE.test.DBConnection;
import JEE.test.TestBean;
import JEE.unitTest.UnitTestBean;
import com.bean.QuestionBean;

public class ViewTestForm extends javax.swing.JFrame {
    DBConnection db1=new DBConnection();
    ArrayList<Integer> E=new ArrayList<Integer>();
    QuestionPanel currentPanel=null;
    ArrayList<QuestionBean> alPhysics,alChemistry,alMaths;
    ArrayList<QuestionBean> alCurrent;
    boolean flagSubject=false,isNewTest=true;
    int currentIndex,quecount[];
    DBConnection db;
    TestBean testBean;
    PracticeBean praBean;
    UnitTestBean uTestBean;
    int animationTime=5;
    private javax.swing.JButton[] jButtonsArrayPhysics,jButtonsArrayMaths,jButtonsArrayChemistry;
    Image bg=new ImageIcon("src/ui/images/wallpaper-1.jpg").getImage();
    JPanel[] queButtonPanels ;
    long remaining; // How many milliseconds remain in the countdown.
    long lastUpdate; // When count was last updated  
    Timer timer; // Updates the count every second
    NumberFormat format;
    int rollNo;
    /** Creates new form TestResultForm */
    public ViewTestForm(int rollNo) {
        initComponents();
        this.rollNo=rollNo;
        this.getContentPane().setBackground(Color.white);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setState(JFrame.MAXIMIZED_BOTH);
        CardLayout cl = (CardLayout) jPanelsSliding1.getLayout();
        cl.show(jPanelsSliding1, "card4");
        currentPanel=questionPanel1;
        db=new DBConnection();
        //setallque();
    }

    public void removeallque()
    {
        pnlAllQue.removeAll();
    }
    
    public void setButtonOnPanel(JPanel panel,JButton[] buttonArray, int count,int subject)
    {        
        for(int x = 0; x < count ; x++){
                buttonArray[x] = new javax.swing.JButton();
                buttonArray[x].setToolTipText("Not Answered");
                QuestionBean q = new QuestionBean();
                if(subject==0){
                    q=alPhysics.get(x);
                }
                else if(subject==1){
                    q=alChemistry.get(x);
                }
                else if(subject==2){
                    q=alMaths.get(x);
                }
                if(q.getUserAnswer().equals("UnAttempted")){
                    buttonArray[x].setBackground(Color.white);
                }
                else{
                    buttonArray[x].setBackground(Color.green);
                }
            
                buttonArray[x].setActionCommand(subject+" "+x);
                buttonArray[x].addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setQuestion(e.getActionCommand());
                    }
                });
                int y=x+1;
                buttonArray[x].setText(""+y);
       }
       GridBagConstraints cons=new GridBagConstraints();
            cons.gridx = 0;   cons.gridy  =  0;
            cons.gridwidth = 1;   cons.gridheight =  1;
            cons.anchor  =  GridBagConstraints.BELOW_BASELINE_LEADING;
            cons.weightx =  2;   cons.weighty =  1;
            cons.insets=new java.awt.Insets(1, 3, 1, 3);
            GridBagLayout layout = new GridBagLayout();
            for(int x=0;x<count;x++)
            {
                if(x%20==0)
                {
                    cons.gridx++;
                    cons.gridy=1;
                }                
                layout.setConstraints(buttonArray[x], cons);
                panel.setLayout(layout);
                panel.add(buttonArray[x],cons);                
                cons.gridy++;
            }
    }
    
    public void createPanelObjects()
    {
        int count=0;
        count=alPhysics.size();                
        if(count>0)
        {
            queButtonPanels[0]=new JPanel();
            for(int m=0;m<count;m++){
                QuestionBean q=alPhysics.get(m);
                if(q.getUserAnswer().equals("UnAttempted")){
                    queButtonPanels[0].setBackground(Color.white);
                }
                else{
                    queButtonPanels[0].setBackground(Color.green);
                }
            }
            jButtonsArrayPhysics=new JButton[count];
            setButtonOnPanel(queButtonPanels[0], jButtonsArrayPhysics, count, 0);            
        }
        else{
            queButtonPanels[0]=null;
        }
        count=alChemistry.size();                
        if(count>0)
        {
            queButtonPanels[1]=new JPanel();
            jButtonsArrayChemistry=new JButton[count];
            setButtonOnPanel(queButtonPanels[1], jButtonsArrayChemistry, count, 1);            
        }
        else{
            queButtonPanels[1]=null;
        }
        count=alMaths.size();                
        if(count>0)
        {
            queButtonPanels[2]=new JPanel();
            for(int m=0;m<count;m++){
                QuestionBean q=alMaths.get(m);
                if(q.getUserAnswer().equals("UnAttempted")){
                    queButtonPanels[2].setBackground(Color.white);
                }
                else{
                    queButtonPanels[2].setBackground(Color.green);
                }
            }
            jButtonsArrayMaths=new JButton[count];
            setButtonOnPanel(queButtonPanels[2], jButtonsArrayMaths, count, 2);            
        }
        else{
            queButtonPanels[2]=null;
        }
    }

    public void createPanelObjects(int subid)
    {
        int count=0;
        if(subid==1){
        count=alPhysics.size();                
        if(count>0)
        {
            queButtonPanels[0]=new JPanel();
            for(int m=0;m<count;m++){
                QuestionBean q=alPhysics.get(m);
                if(q.getUserAnswer().equals("UnAttempted")){
                    queButtonPanels[0].setBackground(Color.white);
                }
                else{
                    queButtonPanels[0].setBackground(Color.green);
                }
            }
            jButtonsArrayPhysics=new JButton[count];
            setButtonOnPanel(queButtonPanels[0], jButtonsArrayPhysics, count, 0);            
        }
        else{
            queButtonPanels[0]=null;
        }}
        else if(subid==2){
        count=alChemistry.size();                
        if(count>0)
        {
            queButtonPanels[1]=new JPanel();
            for(int m=0;m<count;m++){
                QuestionBean q=alChemistry.get(m);
                if(q.getUserAnswer().equals("UnAttempted")){
                    queButtonPanels[1].setBackground(Color.white);
                }
                else{
                    queButtonPanels[1].setBackground(Color.green);
                }
            }
            jButtonsArrayChemistry=new JButton[count];
            setButtonOnPanel(queButtonPanels[1], jButtonsArrayChemistry, count, 1);            
        }
        else{
            queButtonPanels[1]=null;
        }}else if(subid==3){
        count=alMaths.size();                
        if(count>0)
        {
            queButtonPanels[2]=new JPanel();
            for(int m=0;m<count;m++){
                QuestionBean q=alMaths.get(m);
                if(q.getUserAnswer().equals("UnAttempted")){
                    queButtonPanels[2].setBackground(Color.white);
                }
                else{
                    queButtonPanels[2].setBackground(Color.green);
                }
            }
            jButtonsArrayMaths=new JButton[count];
            setButtonOnPanel(queButtonPanels[2], jButtonsArrayMaths, count, 2);            
        }
        else{
            queButtonPanels[2]=null;
        }}
    }
    
    public void setPanel(boolean flag)
    {
        currentPanel=(currentPanel==questionPanel1)?questionPanel2:questionPanel1;
        currentPanel.unlockSelection();    
        //set Question    
         String NumericalAns=""; // not use this form bt temp
        currentPanel.setQuestionOnPanel(alCurrent.get(currentIndex),(currentIndex+1),NumericalAns.trim());
        txtQuestionNumber.setText((currentIndex+1)+"");
        //slide panel    
        jPanelsSliding1.nextSlidPanel(animationTime,currentPanel,flag);
        System.out.println("Time : "+animationTime);
        jPanelsSliding1.refresh();
    }
    
    public void setQuestion(String actionCommand)
    {
        currentIndex=Integer.parseInt(actionCommand.split(" ")[1]);
        setPanel(false);
    }
    
    public ViewTestForm(TestBean testBean,boolean newTest,String name,int rollNO) {
        initComponents();  
        this.rollNo=rollNO;
        this.getContentPane().setBackground(Color.white);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        setTitle(name);
        db=new DBConnection();
        this.testBean=testBean;
        currentPanel=questionPanel1;
        this.alPhysics = testBean.getAlPhysics();
        this.alChemistry = testBean.getAlChemistry();
        this.alMaths = testBean.getAlMaths();
        queButtonPanels = new JPanel[4];
        rdoEnable.setActionCommand("Enable");
        rdoEnable.setActionCommand("Disable");
        btnGroupAnimation.add(rdoEnable);
        btnGroupAnimation.add(rdoDisable);
        
        switch(testBean.getGroupId())
        {
            case 1: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics" }));
                    lblHeading.setText("Test - - ->Physics.");
                    break;
            case 2: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Chemistry" }));
                    lblHeading.setText("Test - - ->Chemistry.");
                    break;
            case 3: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Maths" }));
                    lblHeading.setText("Test - - ->Maths.");
                    break;
            case 4: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics", "Chemistry", "Maths" }));
                    lblHeading.setText("Test - - ->PCM.");
                    break;                
        }

        cmbSubject.setSelectedIndex(-1);   
        txtQuestionNumber.setColumns(3);  
        txtQuestionNumber.setHorizontalAlignment(JTextField.CENTER);
        lastUpdate=System.currentTimeMillis();
        format = NumberFormat.getNumberInstance();
        format.setMinimumIntegerDigits(2);     
        timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateDisplay();
            }
        } );
        createPanelObjects();
        if(newTest)
        {
            db.saveStateOfNewTest(testBean,rollNO);
            
            int minutes= testBean.getTotalTime();
            //System.out.println(minutes+" min");
            if (minutes > 0)
                remaining = minutes * 60000;
            else
                remaining = 600000;
            cmbSubject.setSelectedIndex(0);
        }
        else
        {
            remaining = testBean.getRemainingTime();  
            isNewTest = false;
            cmbSubject.setSelectedIndex(testBean.getCurrentSubjectNumber());
        }
        timer.setInitialDelay(0); 
        timer.start();
    }
   
    public ViewTestForm(UnitTestBean testBean,boolean newTest,boolean newTe,boolean k,int rollNo) {
        initComponents();  
        this.rollNo=rollNo;
        this.getContentPane().setBackground(Color.white);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        db=new DBConnection();
        this.uTestBean=testBean;
        currentPanel=questionPanel1;
        ArrayList<Integer> E=new ArrayList<Integer>();
        queButtonPanels = new JPanel[4];
        rdoEnable.setActionCommand("Enable");
        rdoEnable.setActionCommand("Disable");
        btnGroupAnimation.add(rdoEnable);
        btnGroupAnimation.add(rdoDisable);
        switch(uTestBean.getSubjectId())
        {
            case 1: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics" }));
                    lblHeading.setText("Unit Test - - ->Physics.");
                    break;
            case 2: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Chemistry" }));
                    lblHeading.setText("Unit Test - - ->Chemistry.");
                    break;
            case 3: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Maths" }));
                    lblHeading.setText("Unit Test - - ->Maths.");
                    break;   
            default:cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Unit test" }));
        }

        cmbSubject.setSelectedIndex(-1);   
        txtQuestionNumber.setColumns(3);  
        txtQuestionNumber.setHorizontalAlignment(JTextField.CENTER);
        lastUpdate=System.currentTimeMillis();
        format = NumberFormat.getNumberInstance();
        format.setMinimumIntegerDigits(2);     
        timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateDisplay();
            }
        } );
        if(newTest)
        {
            JEE.unitTest.DBConnection df=new JEE.unitTest.DBConnection();
            df.saveStateOfNewPractice(uTestBean,rollNo);
            createPanelObjects();
            int minutes= testBean.getTotalTime();
            //System.out.println(minutes+" min");
            if (minutes > 0)
                remaining = minutes * 60000;
            else
                remaining = 600000;
            cmbSubject.setSelectedIndex(0);
        }
        else
        {
            createPanelObjects();
            remaining = testBean.getRemainingTime();  
            isNewTest = false;
            cmbSubject.setSelectedIndex(uTestBean.getSubjectId());
        }
        timer.setInitialDelay(0); 
        timer.start();
    }
    
    public ViewTestForm(PracticeBean testBean,boolean newTest,boolean newTe,int rollNo) {
        initComponents();  
        this.rollNo=rollNo;
        this.getContentPane().setBackground(Color.white);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        db=new DBConnection();
        this.praBean=testBean;
        currentPanel=questionPanel1;
        ArrayList<Integer> E=new ArrayList<Integer>();
        queButtonPanels = new JPanel[4];
        rdoEnable.setActionCommand("Enable");
        rdoEnable.setActionCommand("Disable");
        btnGroupAnimation.add(rdoEnable);
        btnGroupAnimation.add(rdoDisable);
        
        switch(praBean.getSubjectId())
        {
            case 1: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics" }));
                    break;
            case 2: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Chemistry" }));
                    break;
            case 3: cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Maths" }));
                    break;
            default:cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Unit test" }));
        }
        cmbSubject.setSelectedIndex(-1);   
        txtQuestionNumber.setColumns(3);  
        txtQuestionNumber.setHorizontalAlignment(JTextField.CENTER);
        lastUpdate=System.currentTimeMillis();
        format = NumberFormat.getNumberInstance();
        format.setMinimumIntegerDigits(2);     
        timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateDisplay();
            }
        } );
        if(newTest)
        {
            JEE.practice.DBConnection df=new JEE.practice.DBConnection();
            df.saveStateOfNewPractice(praBean,rollNo);
            createPanelObjects();
            int minutes= testBean.getTotalTime();
            //System.out.println(minutes+" min");
            if (minutes > 0)
                remaining = minutes * 60000;
            else
                remaining = 600000;
            cmbSubject.setSelectedIndex(0);
        }
        else
        {
            createPanelObjects();
            remaining = testBean.getRemainingTime();  
            isNewTest = false;
            cmbSubject.setSelectedIndex(praBean.getSubjectId());
        }
        timer.setInitialDelay(0); 
        timer.start();
    }

  public class MyKeyListener extends KeyAdapter{
  public void keyPressed(KeyEvent ke){
  char i = ke.getKeyChar();}
  }  
    
  public void start() {
  resume();
  } // Start displaying updates
  public void stop() {
    pause();
  }
  
  void resume() {
    lastUpdate = System.currentTimeMillis();
    timer.start(); // Start the timer
  }

  void pause() {
    long now = System.currentTimeMillis();
    remaining -= (now - lastUpdate);
    timer.stop(); // Stop the timer
  }
  
  void updateDisplay() {
    long now = System.currentTimeMillis(); // current time in ms
    long elapsed = now - lastUpdate; // ms elapsed since last update
    remaining -= elapsed; // adjust remaining time
    lastUpdate = now; // remember this update time
    if (remaining == 0) {
      timer.stop();
      JOptionPane.showMessageDialog(this,"Test Finished"); 
      this.dispose();
      new TestResultForm(testBean,rollNo).setVisible(true);
      }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupAnimation = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        btnFirst = new javax.swing.JButton();
        btnPrevious = new javax.swing.JButton();
        txtQuestionNumber = new javax.swing.JTextField();
        btnNext = new javax.swing.JButton();
        btnLast = new javax.swing.JButton();
        cmbSubject = new javax.swing.JComboBox();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        rdoEnable = new javax.swing.JRadioButton();
        rdoDisable = new javax.swing.JRadioButton();
        btnEndExam = new javax.swing.JButton();
        btnHome = new javax.swing.JButton();
        jPanelsSliding1 = new ui.JPanelsSliding();
        questionPanel1 = new ui.QuestionPanel();
        questionPanel2 = new ui.QuestionPanel();
        pnlAllQue = new javax.swing.JPanel();
        lblHeading = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("CruncherSoft Technologies Pvt. Ltd. NEET+JEE Software 2014");

        jPanel1.setBackground(new java.awt.Color(240, 248, 255));
        jPanel1.setName("jPanel1"); // NOI18N

        btnFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnFirst.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first.png"))); // NOI18N
        btnFirst.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFirst.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnFirst.setName("btnFirst"); // NOI18N
        btnFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFirstActionPerformed(evt);
            }
        });
        jPanel1.add(btnFirst);

        btnPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnPrevious.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/prev.png"))); // NOI18N
        btnPrevious.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPrevious.setIconTextGap(0);
        btnPrevious.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnPrevious.setName("btnPrevious"); // NOI18N
        btnPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreviousActionPerformed(evt);
            }
        });
        jPanel1.add(btnPrevious);

        txtQuestionNumber.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtQuestionNumber.setText("0000");
        txtQuestionNumber.setName("txtQuestionNumber"); // NOI18N
        txtQuestionNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtQuestionNumberKeyReleased(evt);
            }
        });
        jPanel1.add(txtQuestionNumber);

        btnNext.setBackground(new java.awt.Color(255, 255, 255));
        btnNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/next.png"))); // NOI18N
        btnNext.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNext.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnNext.setName("btnNext"); // NOI18N
        btnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });
        jPanel1.add(btnNext);

        btnLast.setBackground(new java.awt.Color(255, 255, 255));
        btnLast.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last.png"))); // NOI18N
        btnLast.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLast.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnLast.setName("btnLast"); // NOI18N
        btnLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLastActionPerformed(evt);
            }
        });
        jPanel1.add(btnLast);

        cmbSubject.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cmbSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Physics", "Chemistry", "Maths" }));
        cmbSubject.setName("cmbSubject"); // NOI18N
        cmbSubject.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSubjectItemStateChanged(evt);
            }
        });
        cmbSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSubjectActionPerformed(evt);
            }
        });
        jPanel1.add(cmbSubject);

        jPanel2.setBackground(new java.awt.Color(240, 255, 255));
        jPanel2.setName("jPanel2"); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Animation :");
        jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel1.setName("jLabel1"); // NOI18N
        jPanel2.add(jLabel1);

        rdoEnable.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        rdoEnable.setSelected(true);
        rdoEnable.setText("Enable");
        rdoEnable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoEnable.setName("rdoEnable"); // NOI18N
        rdoEnable.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoEnableItemStateChanged(evt);
            }
        });
        jPanel2.add(rdoEnable);

        rdoDisable.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        rdoDisable.setText("Disable");
        rdoDisable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoDisable.setName("rdoDisable"); // NOI18N
        rdoDisable.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoDisableItemStateChanged(evt);
            }
        });
        jPanel2.add(rdoDisable);

        btnEndExam.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnEndExam.setText("View Scorecard");
        btnEndExam.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEndExam.setName("btnEndExam"); // NOI18N
        btnEndExam.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEndExamMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnEndExamMouseExited(evt);
            }
        });
        btnEndExam.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                btnEndExamItemStateChanged(evt);
            }
        });
        btnEndExam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEndExamActionPerformed(evt);
            }
        });
        jPanel2.add(btnEndExam);

        btnHome.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnHome.setText("Home");
        btnHome.setName("btnHome"); // NOI18N
        btnHome.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnHomeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnHomeMouseExited(evt);
            }
        });
        btnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHomeActionPerformed(evt);
            }
        });
        jPanel2.add(btnHome);

        jPanelsSliding1.setBackground(new java.awt.Color(255, 255, 255));
        jPanelsSliding1.setName("jPanelsSliding1"); // NOI18N
        jPanelsSliding1.setLayout(new java.awt.CardLayout());

        questionPanel1.setBackground(new java.awt.Color(185, 225, 254));
        questionPanel1.setName("questionPanel1"); // NOI18N
        jPanelsSliding1.add(questionPanel1, "card2");

        questionPanel2.setBackground(new java.awt.Color(185, 225, 254));
        questionPanel2.setName("questionPanel2"); // NOI18N
        jPanelsSliding1.add(questionPanel2, "card3");

        pnlAllQue.setBackground(new java.awt.Color(255, 255, 255));
        pnlAllQue.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pnlAllQue.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pnlAllQue.setName("pnlAllQue"); // NOI18N

        javax.swing.GroupLayout pnlAllQueLayout = new javax.swing.GroupLayout(pnlAllQue);
        pnlAllQue.setLayout(pnlAllQueLayout);
        pnlAllQueLayout.setHorizontalGroup(
            pnlAllQueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 179, Short.MAX_VALUE)
        );
        pnlAllQueLayout.setVerticalGroup(
            pnlAllQueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 312, Short.MAX_VALUE)
        );

        lblHeading.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        lblHeading.setText("Practice - -> Physics - -> Sets");
        lblHeading.setName("lblHeading"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanelsSliding1, javax.swing.GroupLayout.DEFAULT_SIZE, 647, Short.MAX_VALUE)
                        .addGap(10, 10, 10)
                        .addComponent(pnlAllQue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 830, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 830, Short.MAX_VALUE))
                        .addContainerGap())))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblHeading, javax.swing.GroupLayout.DEFAULT_SIZE, 830, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanelsSliding1, 0, 318, Short.MAX_VALUE)
                    .addComponent(pnlAllQue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblHeading)
                    .addContainerGap(422, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFirstActionPerformed
    if(currentIndex!=0)
    {
       currentIndex=0;
        setPanel(true);   
    }
    else
        JOptionPane.showMessageDialog(null,"This is First Question.");
}//GEN-LAST:event_btnFirstActionPerformed

private void btnPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviousActionPerformed
    if(currentIndex!=0)
    {
        currentIndex--;
        setPanel(true);
    }
    else
        JOptionPane.showMessageDialog(null,"This is First Question.");
}//GEN-LAST:event_btnPreviousActionPerformed

private void txtQuestionNumberKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQuestionNumberKeyReleased
if(evt.getKeyCode()==10)
{
    int i = Integer.parseInt(txtQuestionNumber.getText());
    int size = alCurrent.size();
    if((i<1)||(i>size))
    {
        JOptionPane.showMessageDialog(rootPane, "Out Of Range Index");
    }                                     
    else
    {
        currentIndex=i-1;
        setPanel(false);
    }
    }
}//GEN-LAST:event_txtQuestionNumberKeyReleased

public void paintComponent(Graphics g) {
        
        g.drawImage(bg, 0, 0, getWidth(), getHeight(), this);
    }

private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextActionPerformed
    int last=alCurrent.size()-1;
    if(currentIndex<last)
    {
        currentIndex++;
        setPanel(false); 
    }
    else
        JOptionPane.showMessageDialog(null,"This is Last Question.");
}//GEN-LAST:event_btnNextActionPerformed

private void btnLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLastActionPerformed
    int last=alCurrent.size()-1;
    if(currentIndex<last)
    {
        currentIndex=last;
        setPanel(false); 
    }
    else
        JOptionPane.showMessageDialog(null,"This is Last Question.");
}//GEN-LAST:event_btnLastActionPerformed

private void cmbSubjectItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSubjectItemStateChanged
    ui.QuestionPanel setimage=new QuestionPanel();
    pnlAllQue.repaint();
    if(flagSubject)
    {        
        int subjectId;
        QuestionPanel qp=new QuestionPanel();
        String subject = cmbSubject.getSelectedItem().toString();
        if(subject.equals("Physics"))
        {
            pnlAllQue.removeAll();
            alCurrent = alPhysics;
            GridBagLayout lay=new GridBagLayout();
            pnlAllQue.setLayout(lay);
            pnlAllQue.add(queButtonPanels[0]);
        }
        else if(subject.equals("Chemistry"))
        {
            pnlAllQue.removeAll();
            alCurrent = alChemistry;
            GridBagLayout lay=new GridBagLayout();
            pnlAllQue.setLayout(lay);
            pnlAllQue.add(queButtonPanels[1]);
        }
        else if(subject.equals("Maths"))
        {
            pnlAllQue.removeAll();
            alCurrent = alMaths;
            GridBagLayout lay=new GridBagLayout();
            pnlAllQue.setLayout(lay);
            pnlAllQue.add(queButtonPanels[2]);
        }
        else
        {
            alCurrent = null;
            pnlAllQue.removeAll();
        }
        if(alCurrent!=null)
        {
            if(isNewTest)
            {
                currentIndex=0;
            }
            else
            {
                currentIndex = testBean.getCurrentQuestionNumber();
            }                        
            setPanel(false);            
            
        }
        flagSubject=false;
    }
    else
    {
        flagSubject=true;
    }
}//GEN-LAST:event_cmbSubjectItemStateChanged

private void btnEndExamItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_btnEndExamItemStateChanged
// TODO add your handling code here:
}//GEN-LAST:event_btnEndExamItemStateChanged

private void btnEndExamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEndExamActionPerformed
    timer.stop();
    new TestResultForm(testBean,false,false,rollNo).setVisible(true);
    this.dispose();
}//GEN-LAST:event_btnEndExamActionPerformed

private void rdoEnableItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoEnableItemStateChanged
    if(rdoEnable.isSelected())
    {
        animationTime = 5;
    }
    else
    {
        animationTime = 1;
    }    
}//GEN-LAST:event_rdoEnableItemStateChanged

private void rdoDisableItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoDisableItemStateChanged
    if(rdoEnable.isSelected())
    {
        animationTime = 5;
    }
    else
    {
        animationTime = 1;
    }
}//GEN-LAST:event_rdoDisableItemStateChanged

private void btnEndExamMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEndExamMouseEntered
    btnEndExam.setForeground(Color.red);
}//GEN-LAST:event_btnEndExamMouseEntered

private void btnEndExamMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEndExamMouseExited
    btnEndExam.setForeground(Color.black);
}//GEN-LAST:event_btnEndExamMouseExited

    private void cmbSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSubjectActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbSubjectActionPerformed

private void btnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHomeActionPerformed
    this.dispose();
    NewTestForm newTestForm=new NewTestForm(rollNo);
    newTestForm.setVisible(true);
}//GEN-LAST:event_btnHomeActionPerformed

private void btnHomeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHomeMouseEntered
btnHome.setForeground(Color.red);
}//GEN-LAST:event_btnHomeMouseEntered

private void btnHomeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnHomeMouseExited
btnHome.setForeground(Color.black);
}//GEN-LAST:event_btnHomeMouseExited
    public static void main(String args[]) {
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(StartedTestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(StartedTestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(StartedTestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(StartedTestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new StartedTestForm().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEndExam;
    private javax.swing.JButton btnFirst;
    private javax.swing.ButtonGroup btnGroupAnimation;
    private javax.swing.JButton btnHome;
    private javax.swing.JButton btnLast;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnPrevious;
    private javax.swing.JComboBox cmbSubject;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private ui.JPanelsSliding jPanelsSliding1;
    private javax.swing.JLabel lblHeading;
    private javax.swing.JPanel pnlAllQue;
    private ui.QuestionPanel questionPanel1;
    private ui.QuestionPanel questionPanel2;
    private javax.swing.JRadioButton rdoDisable;
    private javax.swing.JRadioButton rdoEnable;
    private javax.swing.JTextField txtQuestionNumber;
    // End of variables declaration//GEN-END:variables
}