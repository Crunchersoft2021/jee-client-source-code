/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * QuestionPanel.java
 *
 * Created on Nov 5, 2012, 2:28:02 PM
 */
package ui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import org.scilab.forge.jlatexmath.*;
import JEE.question.QuestionBean;

/**
 *
 * @author Administrator
 */
public class HintPanel extends javax.swing.JPanel {

    /** Creates new form QuestionPanel */
    String start,end;
    QuestionBean currentQuestion;
    QuestionBean hint;
      
    public HintPanel()
    {
        initComponents();  
    }
    
    public HintPanel(QuestionBean questionBean, int index) {
        initComponents();  
        start = "\\begin{array}{l}";
	end = "\\end{array}";
        
        lblHint.setVisible(false);
        rdoOptionA.setEnabled(false);
        rdoOptionB.setEnabled(false);
        rdoOptionC.setEnabled(false);
        rdoOptionD.setEnabled(false);
        jLabel1.setVisible(false);
        
        setQuestionOnPanel(questionBean, index);
    }
    
    public void showOptions()
    {
                lblA.setVisible(true);
                lblOptionA.setVisible(true);
                lblB.setVisible(true);
                lblOptionB.setVisible(true);
                lblC.setVisible(true);
                lblOptionC.setVisible(true);
                lblD.setVisible(true);
                lblOptionD.setVisible(true);
                lblOptionAsImage.setVisible(false);                
    }
    public void showHint()
    {
        
    }
    public void hideOptions()
    {
                lblA.setVisible(false);
                lblOptionA.setVisible(false);
                lblB.setVisible(false);
                lblOptionB.setVisible(false);
                lblC.setVisible(false);
                lblOptionC.setVisible(false);
                lblD.setVisible(false);
                lblOptionD.setVisible(false);
                lblOptionAsImage.setVisible(true);                
    }
    
    public void setLableText(JLabel l,String str)
    {
        try
        {
            if(str.equals(""))
            {
                
            }
            else
            {
                l.setText("");
                TeXFormula formula;
                TeXIcon icon;
                BufferedImage image;
                Graphics2D g2;                
                JLabel jl;
                str=start+str+end;
                formula = new TeXFormula(str);
                icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);
                icon.setInsets(new Insets(0,0,0,0));
                image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
                g2 = image.createGraphics();
                g2.setColor(Color.white);
                g2.fillRect(0,0,icon.getIconWidth(),icon.getIconHeight());
                jl = new JLabel();
                jl.setForeground(new Color(0, 0, 0));
                icon.paintIcon(jl, g2, 0, 0);
                l.setIcon(icon);   
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,e.getMessage()); 
        }
    }
    
    public void showQuestionImage()
    {         
                lblQuestionAsImage.setVisible(true);                
    }
    public void hideQuestionImage()
    {                
                lblQuestionAsImage.setVisible(false);                
    }
    public void showHintImage()
    {         
                lblHintAsImage.setVisible(true);                
    }
    public void hideHintImage()
    {                
                lblHintAsImage.setVisible(false);                
    }
    public void loadImage(String path,JLabel lbl)
    {   
        BufferedImage image;
        try{            
                File file=new File(path);
                image=ImageIO.read(file);
                ImageIcon icon;
                float width=image.getWidth();
                float height=image.getHeight();
                Image thumb = image.getScaledInstance((int)width,(int)height, Image.SCALE_AREA_AVERAGING);
                icon=new ImageIcon(thumb);
                
                lbl.setIcon(icon);
                lbl.setText("");               
            }        
            catch(Exception e)
            {
                System.out.println(e);
            }
        }
    
    public void setQuestionOnPanel(QuestionBean question,int index)
    {
            currentQuestion=question;
            //hint=question;
            jLabel1.setVisible(false);
            
             if(hint!=null)
            {
                if(question.getHint()=="" || question.getHint()==null)
                {
                    lblHint.setVisible(false);
                    jLabel1.setVisible(false);
                }
                else
                {
                    jLabel1.setVisible(true);
                    lblHint.setVisible(true);
                    setLableText(lblHint,question.getHint());
                }
            }
             
            setLableText(lblQuestion,question.getQuestion());
            if(!question.isIsQuestionAsImage())
            {
                hideQuestionImage();
            }
            else
            {
                showQuestionImage();                
                loadImage(question.getQuestionImagePath(), lblQuestionAsImage);                
            }
            if(!question.isIsHintAsImage())
            {
                hideHintImage();
            }
            else
            {
                showHintImage();                
                loadImage(question.getHintImagePath(), lblHintAsImage);                
            }
            if(!question.isIsOptionAsImage())
            {
                showOptions();                
                setLableText(lblOptionA,question.getA());            
                setLableText(lblOptionB,question.getB());
                setLableText(lblOptionC,question.getC());
                setLableText(lblOptionD,question.getD());
            }
            else
            {
                hideOptions();   
                loadImage(question.getOptionImagePath(), lblOptionAsImage);
            }            
            
            String userAnswer=question.getUserAnswer();
            if(userAnswer.equals("UnAttempted"))
            {                
                lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/unAttempted.png")));
                lblCorrectAnswer.setText(currentQuestion.getAnswer());
            }
            else
            {
                if(userAnswer.equals("A"))
                {
                    rdoOptionA.setSelected(true);
                }
                else if(userAnswer.equals("B"))
                {
                    rdoOptionB.setSelected(true);
                }
                else if(userAnswer.equals("C"))
                {
                    rdoOptionC.setSelected(true);
                }
                if(userAnswer.equals("D"))
                {
                    rdoOptionD.setSelected(true);
                }      
                
                if(userAnswer.equals(currentQuestion.getAnswer()))
                {
                    lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/correct.png")));
                    lblCorrect.setVisible(false);
                    lblCorrectAnswer.setVisible(false);
                    
                }
                else
                {
                    lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/wrong.png")));
                    lblCorrectAnswer.setText(currentQuestion.getAnswer());
                }
            }            
            lblQuestionNo.setText("Q. "+index);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        lblQuestionNo = new javax.swing.JLabel();
        lblQuestion = new javax.swing.JLabel();
        lblQuestionAsImage = new javax.swing.JLabel();
        lblQuestionNo6 = new javax.swing.JLabel();
        lblOptionA = new javax.swing.JLabel();
        lblOptionAsImage = new javax.swing.JLabel();
        lblB = new javax.swing.JLabel();
        lblOptionB = new javax.swing.JLabel();
        lblC = new javax.swing.JLabel();
        lblOptionC = new javax.swing.JLabel();
        lblD = new javax.swing.JLabel();
        lblOptionD = new javax.swing.JLabel();
        lblA = new javax.swing.JLabel();
        rdoOptionD = new javax.swing.JRadioButton();
        lblQuestionNo5 = new javax.swing.JLabel();
        rdoOptionB = new javax.swing.JRadioButton();
        rdoOptionC = new javax.swing.JRadioButton();
        rdoOptionA = new javax.swing.JRadioButton();
        lblCorrect = new javax.swing.JLabel();
        lblCorrectAnswer = new javax.swing.JLabel();
        lblRightWrongSymbol = new javax.swing.JLabel();
        lblHint = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        lblHintAsImage = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setAutoscrolls(true);
        setPreferredSize(new java.awt.Dimension(765, 500));

        lblQuestionNo.setFont(new java.awt.Font("Tahoma", 1, 18));
        lblQuestionNo.setText("Q. 1");
        lblQuestionNo.setName("lblQuestionNo"); // NOI18N

        lblQuestion.setText("Question"); // NOI18N
        lblQuestion.setName("lblQuestion"); // NOI18N

        lblQuestionAsImage.setText("Question Image");
        lblQuestionAsImage.setName("lblQuestionAsImage"); // NOI18N
        lblQuestionAsImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblQuestionAsImageMouseClicked(evt);
            }
        });

        lblQuestionNo6.setFont(new java.awt.Font("Tahoma", 1, 18));
        lblQuestionNo6.setText("Options :");
        lblQuestionNo6.setName("lblQuestionNo6"); // NOI18N

        lblOptionA.setText("jLabel7");
        lblOptionA.setName("lblOptionA"); // NOI18N

        lblOptionAsImage.setText("Option Image");
        lblOptionAsImage.setName("lblOptionAsImage"); // NOI18N

        lblB.setFont(new java.awt.Font("Tahoma", 1, 15));
        lblB.setText("B :");
        lblB.setName("lblB"); // NOI18N

        lblOptionB.setText("jLabel7");
        lblOptionB.setName("lblOptionB"); // NOI18N

        lblC.setFont(new java.awt.Font("Tahoma", 1, 15));
        lblC.setText("C :");
        lblC.setName("lblC"); // NOI18N

        lblOptionC.setText("jLabel7");
        lblOptionC.setName("lblOptionC"); // NOI18N

        lblD.setFont(new java.awt.Font("Tahoma", 1, 15));
        lblD.setText("D :");
        lblD.setName("lblD"); // NOI18N

        lblOptionD.setText("jLabel7");
        lblOptionD.setName("lblOptionD"); // NOI18N

        lblA.setFont(new java.awt.Font("Tahoma", 1, 15));
        lblA.setText("A :");
        lblA.setName("lblA"); // NOI18N

        rdoOptionD.setText("D");
        rdoOptionD.setName("rdoOptionD"); // NOI18N
        rdoOptionD.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionDItemStateChanged(evt);
            }
        });

        lblQuestionNo5.setFont(new java.awt.Font("Tahoma", 1, 18));
        lblQuestionNo5.setText("User Answer:");
        lblQuestionNo5.setName("lblQuestionNo5"); // NOI18N

        rdoOptionB.setText("B");
        rdoOptionB.setName("rdoOptionB"); // NOI18N
        rdoOptionB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionBItemStateChanged(evt);
            }
        });

        rdoOptionC.setText("C");
        rdoOptionC.setName("rdoOptionC"); // NOI18N
        rdoOptionC.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionCItemStateChanged(evt);
            }
        });

        rdoOptionA.setText("A");
        rdoOptionA.setName("rdoOptionA"); // NOI18N
        rdoOptionA.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoOptionAItemStateChanged(evt);
            }
        });

        lblCorrect.setFont(new java.awt.Font("Tahoma", 1, 18));
        lblCorrect.setText("Correct Answer:");
        lblCorrect.setName("lblCorrect"); // NOI18N

        lblCorrectAnswer.setFont(new java.awt.Font("Tahoma", 1, 18));
        lblCorrectAnswer.setText("A");
        lblCorrectAnswer.setName("lblCorrectAnswer"); // NOI18N

        lblRightWrongSymbol.setBackground(new java.awt.Color(255, 255, 255));
        lblRightWrongSymbol.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/correct.png"))); // NOI18N
        lblRightWrongSymbol.setName("lblRightWrongSymbol"); // NOI18N

        lblHint.setForeground(new java.awt.Color(255, 0, 0));
        lblHint.setName("lblHint"); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Hint:");
        jLabel1.setName("jLabel1"); // NOI18N

        lblHintAsImage.setName("lblHintAsImage"); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblQuestionNo)
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblQuestionAsImage)
                            .addComponent(lblQuestion)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblQuestionNo6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblB)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionB))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblA)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionA))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblC)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionC))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblD)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblOptionD))
                            .addComponent(lblOptionAsImage)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblCorrect)
                                    .addComponent(jLabel1))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblHint)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 92, Short.MAX_VALUE)
                                        .addComponent(lblHintAsImage))
                                    .addComponent(lblCorrectAnswer, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(lblQuestionNo5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rdoOptionA)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdoOptionB)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdoOptionC)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rdoOptionD)))
                        .addGap(18, 18, 18)
                        .addComponent(lblRightWrongSymbol)))
                .addContainerGap(411, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo)
                    .addComponent(lblQuestion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblQuestionAsImage)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo6)
                    .addComponent(lblA)
                    .addComponent(lblOptionA))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblB)
                    .addComponent(lblOptionB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblC)
                    .addComponent(lblOptionC))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblD)
                    .addComponent(lblOptionD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblOptionAsImage)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQuestionNo5)
                    .addComponent(rdoOptionA)
                    .addComponent(rdoOptionB)
                    .addComponent(rdoOptionC)
                    .addComponent(rdoOptionD)
                    .addComponent(lblRightWrongSymbol))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblCorrect)
                            .addComponent(lblCorrectAnswer))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblHint)
                            .addComponent(jLabel1)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(lblHintAsImage)))
                .addContainerGap(160, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

private void rdoOptionBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionBItemStateChanged
   
}//GEN-LAST:event_rdoOptionBItemStateChanged

private void rdoOptionDItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionDItemStateChanged
    
}//GEN-LAST:event_rdoOptionDItemStateChanged

private void rdoOptionAItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionAItemStateChanged
    
}//GEN-LAST:event_rdoOptionAItemStateChanged

private void rdoOptionCItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoOptionCItemStateChanged
    
}//GEN-LAST:event_rdoOptionCItemStateChanged

    private void lblQuestionAsImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblQuestionAsImageMouseClicked
        JOptionPane.showMessageDialog(null, "Hello");
        
    }//GEN-LAST:event_lblQuestionAsImageMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblA;
    private javax.swing.JLabel lblB;
    private javax.swing.JLabel lblC;
    private javax.swing.JLabel lblCorrect;
    private javax.swing.JLabel lblCorrectAnswer;
    private javax.swing.JLabel lblD;
    private javax.swing.JLabel lblHint;
    private javax.swing.JLabel lblHintAsImage;
    private javax.swing.JLabel lblOptionA;
    private javax.swing.JLabel lblOptionAsImage;
    private javax.swing.JLabel lblOptionB;
    private javax.swing.JLabel lblOptionC;
    private javax.swing.JLabel lblOptionD;
    private javax.swing.JLabel lblQuestion;
    private javax.swing.JLabel lblQuestionAsImage;
    private javax.swing.JLabel lblQuestionNo;
    private javax.swing.JLabel lblQuestionNo5;
    private javax.swing.JLabel lblQuestionNo6;
    private javax.swing.JLabel lblRightWrongSymbol;
    private javax.swing.JRadioButton rdoOptionA;
    private javax.swing.JRadioButton rdoOptionB;
    private javax.swing.JRadioButton rdoOptionC;
    private javax.swing.JRadioButton rdoOptionD;
    // End of variables declaration//GEN-END:variables
}