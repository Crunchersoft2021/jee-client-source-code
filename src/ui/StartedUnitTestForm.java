/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * StartedUnitTestForm.java
 *
 * Created on May 24, 2013, 10:52:32 AM
 */
package ui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.swing.*;

import JEE.unitTest.DBConnection;
import JEE.unitTest.UnitTestBean;
import com.bean.ClassTestBean;
import com.bean.ImageRatioBean;
import com.bean.QuestionBean;
import com.db.operations.ImageRatioOperation;

/**
 *
 * @author 007
 */
public class StartedUnitTestForm extends javax.swing.JFrame {

    QuestionPanel currentPanel = null;
    ArrayList<QuestionBean> alQuestions;
     ArrayList<QuestionBean> alQuestions_option_phy=new ArrayList<QuestionBean>();
      ArrayList<QuestionBean> alQuestions_numeric_phy=new ArrayList<QuestionBean>();
      ArrayList<QuestionBean> alQuestions_option_chem=new ArrayList<QuestionBean>();
      ArrayList<QuestionBean> alQuestions_numeric_chem=new ArrayList<QuestionBean>();
      ArrayList<QuestionBean> alQuestions_option_maths=new ArrayList<QuestionBean>();
      ArrayList<QuestionBean> alQuestions_numeric_maths=new ArrayList<QuestionBean>();
      
    boolean flagSubject = false, isNewTest = true;
    int currentIndex;
    DBConnection db;
    UnitTestBean unitTestBean;
    JButton jButtonsArray[];
    int animationTime = 5, type = 0;
    int sec = 0;
    int min = 0;
    int hour = 0;
    long remaining; // How many milliseconds remain in the countdown.
    long lastUpdate; // When count was last updated  
    Timer timer; // Updates the count every second
    NumberFormat format;
    ButtonGroup btnGroupAnimation;
    int rollNo;
    private ArrayList<ImageRatioBean> imageRatioList;
     ClassTestBean classTestBean=new ClassTestBean();
     JEE.test.DBConnection con = new JEE.test.DBConnection();
     
    /**
     * Creates new form TestResultForm
     */
    public StartedUnitTestForm(int rollNo) {
        this.rollNo = rollNo;
        btnGroupAnimation = new ButtonGroup();
        setIconImage(new ImageIcon(getClass().getResource("/ui/images/logoscoller.png")).getImage());
        this.setState(JFrame.MAXIMIZED_BOTH);
        CardLayout cl = (CardLayout) jPanelsSliding1.getLayout();
        cl.show(jPanelsSliding1, "card4");
        currentPanel = questionPanel1;
        db = new DBConnection();
        this.getContentPane().setBackground(Color.white);
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(25);
        jLabel1.setVisible(false);
        rdoEnable.setVisible(false);
       // btnClearResponse.setVisible(false);
        rdoDisable.setVisible(false);
    }

    public void setPanel(boolean flag) {
        jLabel1.setVisible(false);
        rdoEnable.setVisible(false);
        rdoDisable.setVisible(false);
      //  btnClearResponse.setVisible(false);
        QuestionBean questionBean = alQuestions.get(currentIndex);
        
        String SubjectName =con.getStudentName(questionBean.getSubjectId());
        String numericalAnswer= con.getnumAnsResult1(unitTestBean.getUnitTestId(),rollNo, questionBean.getQuestionId() );
        lblSubjectName.setText(SubjectName);
        currentPanel = (currentPanel == questionPanel1) ? questionPanel2 : questionPanel1;
        currentPanel.unlockSelection();
        //set Question        
        currentPanel.setQuestionOnPanel(alQuestions.get(currentIndex), (currentIndex + 1),numericalAnswer.trim());
//        currentPanel.setQuestionOnPanel1(questionBean, (currentIndex + 1),imageRatioList);
        txtQuestionNumber.setText((currentIndex + 1) + "");
        //slide panel    
        jPanelsSliding1.nextSlidPanel(animationTime, currentPanel, flag);
        
        if(questionBean.getType()==0||questionBean.getType()==1)
        {
            
                if (questionBean.getUserAnswer().equals("UnAttempted")) {
                    if (questionBean.getView() == 1) {
                        jButtonsArray[currentIndex].setBackground(Color.red);
                    } else {
                        jButtonsArray[currentIndex].setBackground(Color.white);
                    }
                } else {
                    jButtonsArray[currentIndex].setBackground(Color.green);
                }
        }        
        else  if(questionBean.getType()==2)
        {
                
            if (numericalAnswer.equalsIgnoreCase("UnAttempted")) {
                if (questionBean.getView() == 1) {
                    jButtonsArray[currentIndex].setBackground(Color.red);
                } else {
                    jButtonsArray[currentIndex].setBackground(Color.white);
                }
            } else {
                jButtonsArray[currentIndex].setBackground(Color.green);
            }
            
        }    
        System.out.println("Time : " + animationTime);
        jPanelsSliding1.refresh();
    }

    void updateDisplay1() {
        long now = System.currentTimeMillis(); // current time in ms
        long elapsed = now - lastUpdate; // ms elapsed since last update
        remaining -= elapsed; // adjust remaining time
        lastUpdate = now; // remember this update time

        // Convert remaining milliseconds to mm:ss format and display
        if (remaining < 0) {
            remaining = 0;
        }
        int hours = (int) (remaining / 3600000);
        int minutes = (int) ((remaining % 3600000) / 60000);
        int seconds = (int) ((remaining % 60000) / 1000);
        
        lblTimer.setText(format.format(hours) + ":" + format.format(minutes) + ":" + format.format(seconds));
         System.out.println("time1---"+format.format(hours) + ":" + format.format(minutes) + ":" + format.format(seconds));


        // If we've completed the countdown beep and display new page
        if (remaining == 0) {
            // Stop updating now.
            timer.stop();

            JOptionPane.showMessageDialog(this, "Test Finished");
            if (type == 0) {
                timer.stop();
                new PracticeResultForm(unitTestBean, rollNo);
                this.dispose();
            } else if (type == 1) {
                timer.stop();
                new PracticeResultForm(unitTestBean, rollNo, true,classTestBean);
                this.dispose();
            }
            this.dispose();
        }
    }

    void updateDisplay() {
        long now = System.currentTimeMillis(); // current time in ms
        long elapsed = now - lastUpdate; // ms elapsed since last update
        remaining -= elapsed; // adjust remaining time
        lastUpdate = now; // remember this update time

        // Convert remaining milliseconds to mm:ss format and display


        int hours = (int) (remaining / 3600000);
        hours = hours * (-1);
        int minutes = (int) ((remaining % 3600000) / 60000);
        minutes = minutes * (-1);
        int seconds = (int) ((remaining % 60000) / 1000);
        seconds = seconds * (-1);
        lblTimer.setText(format.format(hours) + ":" + format.format(minutes) + ":" + format.format(seconds));
        System.out.println("time2---"+format.format(hours) + ":" + format.format(minutes) + ":" + format.format(seconds));
    }

    public void setQuestion(String actionCommand) {
//        setred();
String numericalAnswer= con.getnumAnsResult1(unitTestBean.getUnitTestId(),rollNo, alQuestions.get(currentIndex).getQuestionId() );
        alQuestions.get(currentIndex).setView(1);
   
        if(alQuestions.get(currentIndex).getType()==0||alQuestions.get(currentIndex).getType()==1)
    {
        if (alQuestions.get(currentIndex).getUserAnswer().equalsIgnoreCase("UnAttempted")) {
            if (alQuestions.get(currentIndex).getView() == 1) {
                jButtonsArray[currentIndex].setBackground(Color.red);
            } else {
                jButtonsArray[currentIndex].setBackground(Color.white);
            }
        } else {
            jButtonsArray[currentIndex].setBackground(Color.green);
        }
    }else if(alQuestions.get(currentIndex).getType()==2)
    {   
        if (numericalAnswer.equalsIgnoreCase("UnAttempted")) {
            if (alQuestions.get(currentIndex).getView() == 1) {
                jButtonsArray[currentIndex].setBackground(Color.red);
            } else {
                jButtonsArray[currentIndex].setBackground(Color.white);
            }
        } else {
            jButtonsArray[currentIndex].setBackground(Color.green);
        }
    } 
        this.validate();
        this.repaint();
        currentIndex = Integer.parseInt(actionCommand.split(" ")[1]);
        System.out.print(unitTestBean.getStatus());
        setPanel(false);
    }

    public void setButtonOnPanel(JPanel queButtonPanels) {
        System.out.println("********Set Button Panel**********");
        int subid = unitTestBean.getSubjectId();
        jButtonsArray = new JButton[unitTestBean.getQuestions().size()];
        for (int x = 0; x < unitTestBean.getQuestions().size(); x++) {
            jButtonsArray[x] = new javax.swing.JButton();
            jButtonsArray[x].setActionCommand(subid + " " + x);
//                if(currentPanel){
            jButtonsArray[x].setBackground(Color.white);
            QuestionBean q = alQuestions.get(x);
            //q.setUserAnswer("UnAttempted");
            System.out.println("q.getUserAnswer()" +q.getUserAnswer());
            String numericalAnswer= con.getnumAnsResult1(unitTestBean.getUnitTestId(),rollNo, q.getQuestionId() );
        if(q.getType()==0||q.getType()==1)
    {   
        //   alQuestions_option.add(q);
            if (q.getUserAnswer().equalsIgnoreCase("UnAttempted")) {
                if (q.getView() == 1) {
                    jButtonsArray[x].setBackground(Color.red);
                } else {
                    jButtonsArray[x].setBackground(Color.white);
                }
            } else {
                jButtonsArray[x].setBackground(Color.green);
            }
    }else if(q.getType()==2)
    {           
      //  alQuestions_numeric.add(q);
            if (numericalAnswer.equalsIgnoreCase("UnAttempted")) {
                if (q.getView() == 1) {
                    jButtonsArray[x].setBackground(Color.red);
                } else {
                    jButtonsArray[x].setBackground(Color.gray);
                }
            } else {
                jButtonsArray[x].setBackground(Color.green);
            }
    }  
            
            jButtonsArray[x].setToolTipText("Not Answered");
            jButtonsArray[x].setSize(50, 50);
            jButtonsArray[x].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setQuestion(e.getActionCommand());
                }
            });
            int y = x + 1;
            jButtonsArray[x].setText("" + y);
        }

        GridBagConstraints cons = new GridBagConstraints();
        cons.gridx = 0;
        cons.gridy = 0;
        cons.gridwidth = 1;
        cons.gridheight = 1;
        cons.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
        cons.weightx = 2;
        cons.weighty = 1;
        cons.insets = new java.awt.Insets(1, 3, 1, 3);
        GridBagLayout layout = new GridBagLayout();
        for (int x = 0; x < unitTestBean.getQuestions().size(); x++) {
            if (x % 15 == 0) {
                cons.gridx++;
                cons.gridy = 1;
            }
            layout.setConstraints(jButtonsArray[x], cons);
            queButtonPanels.setLayout(layout);
            queButtonPanels.add(jButtonsArray[x], cons);
            cons.gridy++;
        }
    }

    public StartedUnitTestForm(UnitTestBean unitTestBean, boolean newTest, int rollNo) {
        initComponents();
        type = 0;
        this.rollNo = rollNo;
        this.getContentPane().setBackground(Color.white);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        jScrollPane1.getVerticalScrollBar().setUnitIncrement(25);
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        btnGroupAnimation = new ButtonGroup();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        db = new DBConnection();
        this.unitTestBean = unitTestBean;

        currentPanel = questionPanel1;
        this.alQuestions = unitTestBean.getQuestions();
        CardLayout cl = (CardLayout) jPanelsSliding1.getLayout();
        //cl.show(jPanelsSliding1, "card4");

        rdoEnable.setActionCommand("Enable");
        rdoEnable.setActionCommand("Disable");
        btnGroupAnimation.add(rdoEnable);
        btnGroupAnimation.add(rdoDisable);

        txtQuestionNumber.setColumns(3);
        txtQuestionNumber.setHorizontalAlignment(JTextField.CENTER);

        lastUpdate = System.currentTimeMillis();
        format = NumberFormat.getNumberInstance();
        format.setMinimumIntegerDigits(2);
        timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateDisplay();
            }
        });
        if (newTest) {
            db.saveStateOfNewPractice(unitTestBean, rollNo);
             int minutes = 0;
            if (minutes == 0) {
                remaining = minutes * 60000;
           
            } else {
                remaining = 600000;
            }
            currentIndex = 0;
        } else {
            remaining = unitTestBean.getRemainingTime();
            currentIndex = unitTestBean.getCurrentQuestionNumber();
        }
        timer.setInitialDelay(0);
        timer.start();
        setButtonOnPanel(pnlAllQue);
        setPanel(false);
    }

    public StartedUnitTestForm(UnitTestBean unitTestBean, boolean newTest, int rollNo, boolean just,ClassTestBean classTestBean) {
      
        initComponents();
        this.classTestBean=classTestBean;
        imageRatioList = new ImageRatioOperation().getImageRatioList();
        System.out.println("Start Window");
        type = 1;
        this.rollNo = rollNo;
        this.getContentPane().setBackground(Color.white);
        String titl = new SetFrameTitle().setTitle();
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        btnGroupAnimation = new ButtonGroup();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        db = new DBConnection();
        this.unitTestBean = unitTestBean;

        jScrollPane1.getVerticalScrollBar().setUnitIncrement(25);
        currentPanel = questionPanel1;
        this.alQuestions = unitTestBean.getQuestions();
        System.out.println("unitTestBean.getQuestions()----->"+unitTestBean.getQuestions().size());
       // Edited by Poonam
        int simple =0,nume=0;
           
        
        
          for(int i=0;i<=unitTestBean.getQuestions().size()-1;i++)
          {
              QuestionBean q=alQuestions.get(i);
            
           if(( q.getType()==0|| q.getType()==1) && q.getSubjectId()==1)
           {
               
               simple++;
          
               
               alQuestions_option_phy.add(q);
               
         
               
           }else if(q.getType()==2 && q.getSubjectId()==1)
           {
               alQuestions_numeric_phy.add(q);
               nume++;
               
               
           }
            if((q.getType()==0 || q.getType()==1) && q.getSubjectId()==2)
           {
               alQuestions_option_chem.add(q);
               
           }else if(q.getType()==2 && q.getSubjectId()==2)
           {
               alQuestions_numeric_chem.add(q);
           }
             if((q.getType()==0 || q.getType()==1) && q.getSubjectId()==3)
           {
               alQuestions_option_maths.add(q);
               
           }else if(q.getType()==2 && q.getSubjectId()==3)
           {
               alQuestions_numeric_maths.add(q);
           }
               
        }
          System.out.println("-------------------UK------------------------------------"+simple);
           System.out.println("-------------------UK------------------------------------"+nume);
      alQuestions.clear();
      System.out.println("alQuestions_option_phy=========="+alQuestions_option_phy.size());
      System.out.println("alQuestions_numeric_phy=========="+alQuestions_numeric_phy.size());
      System.out.println("alQuestions_option_chem=========="+alQuestions_option_chem.size());
       
      System.out.println("alQuestions_numeric_chem=========="+alQuestions_numeric_chem.size());
      System.out.println("alQuestions_option_maths=========="+alQuestions_option_maths.size());
     
      System.out.println("alQuestions_numeric_maths=========="+alQuestions_numeric_maths.size());
       
      alQuestions_option_phy.addAll(alQuestions_option_phy.size(),alQuestions_numeric_phy);
      alQuestions_option_chem.addAll(alQuestions_option_chem.size(),alQuestions_numeric_chem);
      alQuestions_option_maths.addAll(alQuestions_option_maths.size(),alQuestions_numeric_maths);
      
      alQuestions.addAll(alQuestions_option_phy);
      //System.out.println("alQuestions.size()=========="+alQuestions.size());
      
      alQuestions.addAll(alQuestions_option_chem);       
      //System.out.println("alQuestions.size()=========="+alQuestions.size());
       
      alQuestions.addAll(alQuestions_option_maths);
      // System.out.println("alQuestions.size()=========="+alQuestions.size());
      
        CardLayout cl = (CardLayout) jPanelsSliding1.getLayout();
        //cl.show(jPanelsSliding1, "card4");

        rdoEnable.setActionCommand("Enable");
        rdoEnable.setActionCommand("Disable");
        btnGroupAnimation.add(rdoEnable);
        btnGroupAnimation.add(rdoDisable);

        txtQuestionNumber.setColumns(3);
        txtQuestionNumber.setHorizontalAlignment(JTextField.CENTER);

        lastUpdate = System.currentTimeMillis();
        format = NumberFormat.getNumberInstance();
        format.setMinimumIntegerDigits(2);
        timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateDisplay1();            }

        });
        if (newTest) {
            db.saveStateOfNewTest(unitTestBean, rollNo);
            int minutes = unitTestBean.getTotalTime();
            System.out.println(minutes+" min");
            if (minutes > 0) {
                remaining = minutes * 60000;

            } else {
               remaining = 600000;
            }
        } else {
            remaining = unitTestBean.getRemainingTime();
           
            isNewTest = false;
        }

        timer.setInitialDelay(0);
        timer.start();
        setButtonOnPanel(pnlAllQue);
        setPanel(false);
    }

    public void start() {
        resume();
    } // Start displaying updates
    // The browser calls this to stop the applet. It may be restarted later.
    // The pause() method is defined below

    public void stop() {
        pause();
    }
    // Start or resume the countdown

    void resume() {
        // Restore the time we're counting down from and restart the timer.
        lastUpdate = System.currentTimeMillis();
        timer.start(); // Start the timer
    }
    // Pause the countdown

    void pause() {
        // Subtract elapsed time from the remaining time and stop timing
        long now = System.currentTimeMillis();
        remaining -= (now - lastUpdate);
        timer.stop(); // Stop the timer
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        rdoEnable = new javax.swing.JRadioButton();
        rdoDisable = new javax.swing.JRadioButton();
        lblTimer = new javax.swing.JLabel();
        btnClearResponse = new javax.swing.JButton();
        btnSubmit = new javax.swing.JButton();
        btnEndPractice = new javax.swing.JButton();
        lblSubjectName = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnFirst = new javax.swing.JButton();
        btnPrevious = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        txtQuestionNumber = new javax.swing.JTextField();
        btnNext = new javax.swing.JButton();
        btnLast = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        UnAttempted = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanelsSliding1 = new ui.JPanelsSliding();
        questionPanel2 = new ui.QuestionPanel();
        questionPanel1 = new ui.QuestionPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        pnlAllQue = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("CruncherSoft's NEET+JEE Software 2014");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel4.setBackground(new java.awt.Color(29, 9, 44));
        jPanel4.setName("jPanel4"); // NOI18N

        jLabel1.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Animation :");
        jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel1.setName("jLabel1"); // NOI18N
        jPanel4.add(jLabel1);

        rdoEnable.setBackground(new java.awt.Color(29, 9, 44));
        rdoEnable.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        rdoEnable.setForeground(new java.awt.Color(255, 255, 255));
        rdoEnable.setSelected(true);
        rdoEnable.setText("Enable");
        rdoEnable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoEnable.setName("rdoEnable"); // NOI18N
        rdoEnable.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoEnableItemStateChanged(evt);
            }
        });
        jPanel4.add(rdoEnable);

        rdoDisable.setBackground(new java.awt.Color(29, 9, 44));
        rdoDisable.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        rdoDisable.setForeground(new java.awt.Color(255, 255, 255));
        rdoDisable.setText("Disable");
        rdoDisable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        rdoDisable.setName("rdoDisable"); // NOI18N
        rdoDisable.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rdoDisableItemStateChanged(evt);
            }
        });
        jPanel4.add(rdoDisable);

        lblTimer.setBackground(new java.awt.Color(29, 9, 44));
        lblTimer.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        lblTimer.setForeground(new java.awt.Color(255, 255, 255));
        lblTimer.setText("jLabel1");
        lblTimer.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        lblTimer.setName("lblTimer"); // NOI18N
        lblTimer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblTimerMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblTimerMouseExited(evt);
            }
        });
        jPanel4.add(lblTimer);

        btnClearResponse.setBackground(new java.awt.Color(255, 255, 255));
        btnClearResponse.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnClearResponse.setForeground(new java.awt.Color(29, 9, 44));
        btnClearResponse.setText("Clear Response");
        btnClearResponse.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnClearResponse.setName("btnClearResponse"); // NOI18N
        btnClearResponse.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                btnClearResponseItemStateChanged(evt);
            }
        });
        btnClearResponse.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnClearResponseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnClearResponseMouseExited(evt);
            }
        });
        btnClearResponse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearResponseActionPerformed(evt);
            }
        });
        jPanel4.add(btnClearResponse);

        btnSubmit.setBackground(new java.awt.Color(255, 255, 255));
        btnSubmit.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnSubmit.setForeground(new java.awt.Color(29, 9, 44));
        btnSubmit.setText("Submit & Next");
        btnSubmit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSubmit.setName("btnSubmit"); // NOI18N
        btnSubmit.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                btnSubmitItemStateChanged(evt);
            }
        });
        btnSubmit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSubmitMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSubmitMouseExited(evt);
            }
        });
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });
        btnSubmit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnSubmitKeyPressed(evt);
            }
        });
        jPanel4.add(btnSubmit);

        btnEndPractice.setBackground(new java.awt.Color(255, 255, 255));
        btnEndPractice.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnEndPractice.setForeground(new java.awt.Color(29, 9, 44));
        btnEndPractice.setText("End Test");
        btnEndPractice.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEndPractice.setName("btnEndPractice"); // NOI18N
        btnEndPractice.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                btnEndPracticeItemStateChanged(evt);
            }
        });
        btnEndPractice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEndPracticeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnEndPracticeMouseExited(evt);
            }
        });
        btnEndPractice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEndPracticeActionPerformed(evt);
            }
        });
        jPanel4.add(btnEndPractice);

        lblSubjectName.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblSubjectName.setForeground(new java.awt.Color(255, 255, 255));
        lblSubjectName.setText("Subject ");
        lblSubjectName.setName("lblSubjectName"); // NOI18N
        jPanel4.add(lblSubjectName);

        jPanel2.setBackground(new java.awt.Color(29, 9, 44));
        jPanel2.setName("jPanel2"); // NOI18N

        btnFirst.setBackground(new java.awt.Color(255, 255, 255));
        btnFirst.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/first.png"))); // NOI18N
        btnFirst.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFirst.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnFirst.setName("btnFirst"); // NOI18N
        btnFirst.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnFirstMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnFirstMouseExited(evt);
            }
        });
        btnFirst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFirstActionPerformed(evt);
            }
        });
        jPanel2.add(btnFirst);

        btnPrevious.setBackground(new java.awt.Color(255, 255, 255));
        btnPrevious.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/prev.png"))); // NOI18N
        btnPrevious.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPrevious.setIconTextGap(0);
        btnPrevious.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnPrevious.setName("btnPrevious"); // NOI18N
        btnPrevious.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPreviousMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPreviousMouseExited(evt);
            }
        });
        btnPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreviousActionPerformed(evt);
            }
        });
        jPanel2.add(btnPrevious);

        jPanel1.setBackground(new java.awt.Color(29, 9, 44));
        jPanel1.setName("jPanel1"); // NOI18N

        txtQuestionNumber.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtQuestionNumber.setForeground(new java.awt.Color(29, 9, 44));
        txtQuestionNumber.setText("0000");
        txtQuestionNumber.setName("txtQuestionNumber"); // NOI18N
        txtQuestionNumber.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtQuestionNumberMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                txtQuestionNumberMouseExited(evt);
            }
        });
        txtQuestionNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtQuestionNumberKeyReleased(evt);
            }
        });
        jPanel1.add(txtQuestionNumber);

        btnNext.setBackground(new java.awt.Color(255, 255, 255));
        btnNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/next.png"))); // NOI18N
        btnNext.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNext.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnNext.setName("btnNext"); // NOI18N
        btnNext.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNextMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnNextMouseExited(evt);
            }
        });
        btnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });
        jPanel1.add(btnNext);

        jPanel2.add(jPanel1);

        btnLast.setBackground(new java.awt.Color(255, 255, 255));
        btnLast.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/images/last.png"))); // NOI18N
        btnLast.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLast.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnLast.setName("btnLast"); // NOI18N
        btnLast.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnLastMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnLastMouseExited(evt);
            }
        });
        btnLast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLastActionPerformed(evt);
            }
        });
        jPanel2.add(btnLast);

        jPanel3.setName("jPanel3"); // NOI18N
        jPanel3.setPreferredSize(new java.awt.Dimension(695, 5));

        jLabel2.setText("UnAttempted");
        jLabel2.setName("jLabel2"); // NOI18N

        UnAttempted.setBackground(new java.awt.Color(255, 0, 0));
        UnAttempted.setName("UnAttempted"); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 833, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(384, 384, 384)
                    .addComponent(jLabel2)
                    .addContainerGap(385, Short.MAX_VALUE)))
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(408, 408, 408)
                    .addComponent(UnAttempted, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(409, Short.MAX_VALUE)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(7, 7, 7)
                    .addComponent(jLabel2)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(9, 9, 9)
                    .addComponent(UnAttempted, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jScrollPane1.setBackground(new java.awt.Color(29, 9, 44));
        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jPanelsSliding1.setBackground(new java.awt.Color(255, 255, 255));
        jPanelsSliding1.setName("jPanelsSliding1"); // NOI18N
        jPanelsSliding1.setLayout(new java.awt.CardLayout());

        questionPanel2.setName("questionPanel2"); // NOI18N
        jPanelsSliding1.add(questionPanel2, "card3");

        questionPanel1.setFont(new java.awt.Font("Centaur", 0, 11)); // NOI18N
        questionPanel1.setName("questionPanel1"); // NOI18N
        questionPanel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                questionPanel1KeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                questionPanel1KeyTyped(evt);
            }
        });
        jPanelsSliding1.add(questionPanel1, "card2");

        jScrollPane1.setViewportView(jPanelsSliding1);

        jScrollPane2.setBackground(new java.awt.Color(29, 9, 44));
        jScrollPane2.setName("jScrollPane2"); // NOI18N

        pnlAllQue.setBackground(new java.awt.Color(255, 255, 255));
        pnlAllQue.setName("pnlAllQue"); // NOI18N

        javax.swing.GroupLayout pnlAllQueLayout = new javax.swing.GroupLayout(pnlAllQue);
        pnlAllQue.setLayout(pnlAllQueLayout);
        pnlAllQueLayout.setHorizontalGroup(
            pnlAllQueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 133, Short.MAX_VALUE)
        );
        pnlAllQueLayout.setVerticalGroup(
            pnlAllQueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 367, Short.MAX_VALUE)
        );

        jScrollPane2.setViewportView(pnlAllQue);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 833, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 692, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 833, Short.MAX_VALUE))
                .addGap(0, 0, 0))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 833, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 364, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 437, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void rdoEnableItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoEnableItemStateChanged
    if (rdoEnable.isSelected()) {
        animationTime = 5;
    } else {
        animationTime = 1;
    }
}//GEN-LAST:event_rdoEnableItemStateChanged

private void rdoDisableItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rdoDisableItemStateChanged
    if (rdoEnable.isSelected()) {
        animationTime = 5;
    } else {
        animationTime = 1;
    }
}//GEN-LAST:event_rdoDisableItemStateChanged

private void btnSubmitMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseEntered
    btnSubmit.setForeground(Color.red);
    //lblMsg.setText("Submit Answer And View Next Question.");
}//GEN-LAST:event_btnSubmitMouseEntered

private void btnSubmitMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSubmitMouseExited
    btnSubmit.setForeground(Color.black);
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_btnSubmitMouseExited

private void btnSubmitItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_btnSubmitItemStateChanged
}//GEN-LAST:event_btnSubmitItemStateChanged

private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
    if (type == 0) {
        int last = alQuestions.size() - 1;
        
        String userAnswer = currentPanel.getUserAnswer();
        
        
        int i = currentIndex;
        if (userAnswer == null ) {
            JOptionPane.showMessageDialog(rootPane, "Please Select Option");
        } else {
            if (i < last) {
                db.setQuestionStatus(alQuestions.get(currentIndex), unitTestBean.getUnitTestId(), userAnswer.trim(),rollNo);
                jButtonsArray[i].setBackground(Color.green);
                jButtonsArray[i].setToolTipText("Answered");
                btnNextActionPerformed(evt);
            } else if (i == last) {
                db.setQuestionStatus(alQuestions.get(currentIndex), unitTestBean.getUnitTestId(), userAnswer,rollNo);
                jButtonsArray[i].setBackground(Color.green);
                jButtonsArray[i].setToolTipText("Answered");
                JOptionPane.showMessageDialog(null, "Last Question is Submitted.");
                boolean b = true;
                btnNextActionPerformed(evt, b);
            }
        }
    } else if (type == 1) {
        JEE.unitTest.DBConnection con=null;
        QuestionBean questionBean =alQuestions.get(currentIndex);
        int last = alQuestions.size() - 1;
        String userAnswer = currentPanel.getUserAnswer();
//        String numericalAnswer= con.getnumAnsResult1(unitTestBean.getUnitTestId(),rollNo, questionBean.getQuestionId() );
        String NumericalAnswer=currentPanel.getNumericalAnswer().trim();
        
        int i = currentIndex;
        
         if(questionBean.getType()==0||questionBean.getType()==1)
        {
                if (userAnswer == null ) {
                    JOptionPane.showMessageDialog(rootPane, "Please Select Option");
                } else {
                    if (i < last) {
                        db.setClassQuestionStatus(alQuestions.get(currentIndex), unitTestBean.getUnitTestId(), userAnswer,rollNo,NumericalAnswer.trim());
                        jButtonsArray[i].setBackground(Color.green);
                        jButtonsArray[i].setToolTipText("Answered");
                        btnNextActionPerformed(evt);
//                        currentPanel.clearTxt();
                    } else if (i == last) {
                        db.setClassQuestionStatus(alQuestions.get(currentIndex), unitTestBean.getUnitTestId(), userAnswer,rollNo,NumericalAnswer.trim());
                        jButtonsArray[i].setBackground(Color.green);
                        jButtonsArray[i].setToolTipText("Answered");
                        JOptionPane.showMessageDialog(null, "Last Question is Submitted.");
                        boolean b = true;
                        btnNextActionPerformed(evt, b);
//                        currentPanel.clearTxt();
                    }
                }
        }        
        else if(questionBean.getType()==2)
        { 
            if (NumericalAnswer.equalsIgnoreCase("UnAttempted") ){
                    JOptionPane.showMessageDialog(rootPane, "Please Enter Numerical Answer");
                } else {
                    if (i < last) {
                        db.setClassQuestionStatus(alQuestions.get(currentIndex), unitTestBean.getUnitTestId(), userAnswer,rollNo,NumericalAnswer.trim());
                        jButtonsArray[i].setBackground(Color.green);
                        jButtonsArray[i].setToolTipText("Answered");
                        btnNextActionPerformed(evt,NumericalAnswer.trim());
//                        currentPanel.clearTxt();
                    } else if (i == last) {
                        db.setClassQuestionStatus(alQuestions.get(currentIndex), unitTestBean.getUnitTestId(), userAnswer,rollNo,NumericalAnswer.trim());
                        jButtonsArray[i].setBackground(Color.green);
                        jButtonsArray[i].setToolTipText("Answered");
                        JOptionPane.showMessageDialog(null, "Last Question is Submitted.");
                        boolean b = true;
                        btnNextActionPerformed(evt, b);
//                        currentPanel.clearTxt();
                    }
                }
        }
    }   
         
         
}//GEN-LAST:event_btnSubmitActionPerformed

private void btnSubmitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnSubmitKeyPressed
}//GEN-LAST:event_btnSubmitKeyPressed

private void btnEndPracticeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEndPracticeMouseEntered
    btnEndPractice.setForeground(Color.red);
    //lblMsg.setText("End The Test And View Result.");
}//GEN-LAST:event_btnEndPracticeMouseEntered

private void btnEndPracticeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEndPracticeMouseExited
    btnEndPractice.setForeground(Color.black);
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_btnEndPracticeMouseExited

private void btnEndPracticeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_btnEndPracticeItemStateChanged
// TODO add your handling code here:
}//GEN-LAST:event_btnEndPracticeItemStateChanged

private void btnEndPracticeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEndPracticeActionPerformed
    Object[] options = {"YES", "CANCEL"};
    JEE.unitTest.DBConnection con = new JEE.unitTest.DBConnection();
    int i = JOptionPane.showOptionDialog(null, "Are You Sure to Quit the Test?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
    if (i == 0) {
        SaveTestStatus();
        System.out.println("type="+type);
        this.dispose();
        if (type == 0) {
            timer.stop();
            new PracticeResultForm(unitTestBean, rollNo);
            this.dispose();
        } else if (type == 1) {
            timer.stop();
            if(unitTestBean.getSubjectId()==0)
            {
                 int grpId =0;
                 System.out.println("Groupwise");
                 new PracticeResultForm(unitTestBean, rollNo, true,grpId ,classTestBean);
            }
            else
            {
                 System.out.println("Chaper/SubjectWise");
                 new PracticeResultForm(unitTestBean, rollNo, true,classTestBean);   
            }
            System.out.println("12345*********");
            this.dispose();
        }
    }
}//GEN-LAST:event_btnEndPracticeActionPerformed

private void btnFirstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFirstActionPerformed
    String NumericalAnswer=currentPanel.getNumericalAnswer().trim();
    if (currentIndex != 0) {
        alQuestions.get(currentIndex).setView(1);
    if(alQuestions.get(currentIndex).getType()==0||alQuestions.get(currentIndex).getType()==1)
    {    
        if (alQuestions.get(currentIndex).getUserAnswer().equalsIgnoreCase("UnAttempted")) {
            if (alQuestions.get(currentIndex).getView() == 1) {
                jButtonsArray[currentIndex].setBackground(Color.red);
            } else {
                jButtonsArray[currentIndex].setBackground(Color.white);
            }
        } else {
            jButtonsArray[currentIndex].setBackground(Color.green);
        }
    } else
    if(alQuestions.get(currentIndex).getType()==2)
    {     
        if (NumericalAnswer.equalsIgnoreCase("UnAttempted")) {
            if (alQuestions.get(currentIndex).getView() == 1) {
                jButtonsArray[currentIndex].setBackground(Color.red);
            } else {
                jButtonsArray[currentIndex].setBackground(Color.white);
            }
        } else {
            jButtonsArray[currentIndex].setBackground(Color.green);
        }
    }       
//        setred();
        currentIndex = 0;
        setPanel(true);
    } else {
        JOptionPane.showMessageDialog(null, "This is First Question.");
    }
}//GEN-LAST:event_btnFirstActionPerformed

private void btnPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviousActionPerformed
    if (currentIndex != 0) {
         String NumericalAnswer=currentPanel.getNumericalAnswer().trim();
        alQuestions.get(currentIndex).setView(1);
    if(alQuestions.get(currentIndex).getType()==0||alQuestions.get(currentIndex).getType()==1)
    {    
        if (alQuestions.get(currentIndex).getUserAnswer().equalsIgnoreCase("UnAttempted")) {
            if (alQuestions.get(currentIndex).getView() == 1) {
                jButtonsArray[currentIndex].setBackground(Color.red);
            } else {
                jButtonsArray[currentIndex].setBackground(Color.white);
            }
        } else {
            jButtonsArray[currentIndex].setBackground(Color.green);
        }
        
    }
    else
    if(alQuestions.get(currentIndex).getType()==2)
    {    
        if (NumericalAnswer.equalsIgnoreCase("UnAttempted")) {
            if (alQuestions.get(currentIndex).getView() == 1) {
                //jButtonsArray[currentIndex].setBackground(Color.red);
            } else {
                jButtonsArray[currentIndex].setBackground(Color.white);
            }
        } else {
            jButtonsArray[currentIndex].setBackground(Color.green);
        }
    }    
//        setred();
        currentIndex--;
        setPanel(true);
    } else {
        JOptionPane.showMessageDialog(null, "This is First Question.");
    }
}//GEN-LAST:event_btnPreviousActionPerformed

private void txtQuestionNumberKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtQuestionNumberKeyReleased
    if (evt.getKeyCode() == 10) {
        int i = Integer.parseInt(txtQuestionNumber.getText());
        int size = alQuestions.size();
        if ((i < 1) || (i > size)) {
            JOptionPane.showMessageDialog(rootPane, "Out Of Range Index");
        } else {
//                setred();
            currentIndex = i - 1;
            setPanel(false);
        }
    }
}//GEN-LAST:event_txtQuestionNumberKeyReleased
    private void btnNextActionPerformed(java.awt.event.ActionEvent evt, boolean b) {
        int last = alQuestions.size() - 1;
        if (currentIndex == last) {
//        setred();
            currentIndex = 0;
            setPanel(false);
        } else {
            JOptionPane.showMessageDialog(null, "This is Last Question.");
        }
    }
private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextActionPerformed
    int last = alQuestions.size() - 1;
//    String NumericalAnswer=currentPanel.getNumericalAnswer();
    
  String NumericalAnswer= con.getnumAnsResult1(unitTestBean.getUnitTestId(),rollNo, alQuestions.get(currentIndex).getQuestionId() );
  System.out.println("NumericalAnswer="+NumericalAnswer.trim());
alQuestions.get(currentIndex).setView(1);
    if(alQuestions.get(currentIndex).getType()==0||alQuestions.get(currentIndex).getType()==1)
    {
            if (alQuestions.get(currentIndex).getUserAnswer().equalsIgnoreCase("UnAttempted")) {
                if (alQuestions.get(currentIndex).getView() == 1) {
                    jButtonsArray[currentIndex].setBackground(Color.red);
                } else {
                    jButtonsArray[currentIndex].setBackground(Color.white);
                }
            } else {
                jButtonsArray[currentIndex].setBackground(Color.green);
            }
    }
    else
    if(alQuestions.get(currentIndex).getType()==2)
    {    
            if (NumericalAnswer.equalsIgnoreCase("UnAttempted") ) {
               
                if (alQuestions.get(currentIndex).getView() == 1) {
                    jButtonsArray[currentIndex].setBackground(Color.red);
                } else {
                    jButtonsArray[currentIndex].setBackground(Color.white);
                }
            } else {
                jButtonsArray[currentIndex].setBackground(Color.green);
            }
    }
    if (currentIndex < last) {
//        setred();
        currentIndex++;
        setPanel(false);
    } else {
        JOptionPane.showMessageDialog(null, "This is Last Question.");
    }
}//GEN-LAST:event_btnNextActionPerformed
private void btnNextActionPerformed(java.awt.event.ActionEvent evt,String NumericalAnswer) {                                        
    int last = alQuestions.size() - 1;
    alQuestions.get(currentIndex).setView(1);
    if(alQuestions.get(currentIndex).getType()==0||alQuestions.get(currentIndex).getType()==1)
    {
         
            if (alQuestions.get(currentIndex).getUserAnswer().equalsIgnoreCase("UnAttempted")) {
                if (alQuestions.get(currentIndex).getView() == 1) {
                    jButtonsArray[currentIndex].setBackground(Color.red);
                } else {
                    jButtonsArray[currentIndex].setBackground(Color.white);
                }
            } else {
                jButtonsArray[currentIndex].setBackground(Color.green);
            }
    } 
    else if(alQuestions.get(currentIndex).getType()==2)
    {
         if (NumericalAnswer.equalsIgnoreCase("UnAttempted")) {
            if (alQuestions.get(currentIndex).getView() == 1) {
                jButtonsArray[currentIndex].setBackground(Color.red);
            } else {
                jButtonsArray[currentIndex].setBackground(Color.white);
            }
        } else {
            jButtonsArray[currentIndex].setBackground(Color.green);
        }
    } 
    if (currentIndex < last) {
//        setred();
        currentIndex++;
        setPanel(false);
    } else {
        JOptionPane.showMessageDialog(null, "This is Last Question.");
    }
}     
private void btnLastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLastActionPerformed
    
    String NumericalAnswer=currentPanel.getNumericalAnswer().trim();
    int last = alQuestions.size() - 1;
    alQuestions.get(currentIndex).setView(1);
    if(alQuestions.get(currentIndex).getType()==0||alQuestions.get(currentIndex).getType()==1)
    {
        if (alQuestions.get(currentIndex).getUserAnswer().equalsIgnoreCase("UnAttempted")) {
            if (alQuestions.get(currentIndex).getView() == 1) {
                jButtonsArray[currentIndex].setBackground(Color.red);
            } else {
                jButtonsArray[currentIndex].setBackground(Color.white);
            }
        } else {
            jButtonsArray[currentIndex].setBackground(Color.green);
        }
    }    
    else if(alQuestions.get(currentIndex).getType()==2)
    {
     
            if (NumericalAnswer.equalsIgnoreCase("UnAttempted")) {
                if (alQuestions.get(currentIndex).getView() == 1) {
                    jButtonsArray[currentIndex].setBackground(Color.red);
                } else {
                    jButtonsArray[currentIndex].setBackground(Color.white);
                }
            } else {
                jButtonsArray[currentIndex].setBackground(Color.green);
            }
            
   }        
    if (currentIndex < last) {
//        setred();
        currentIndex = last;
        setPanel(false);
    } else {
        JOptionPane.showMessageDialog(null, "This is Last Question.");
    }
}//GEN-LAST:event_btnLastActionPerformed

private void lblTimerMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTimerMouseEntered
    //lblMsg.setText("Show Time Of Test.");
}//GEN-LAST:event_lblTimerMouseEntered

private void lblTimerMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblTimerMouseExited
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_lblTimerMouseExited

private void btnFirstMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnFirstMouseEntered
    //lblMsg.setText("Switch And View To First Question.");
}//GEN-LAST:event_btnFirstMouseEntered

private void btnFirstMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnFirstMouseExited
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_btnFirstMouseExited

private void btnPreviousMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPreviousMouseEntered
    //lblMsg.setText("Switch And View To Previous Question.");
}//GEN-LAST:event_btnPreviousMouseEntered

private void txtQuestionNumberMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtQuestionNumberMouseEntered
    //lblMsg.setText("Enter Question No. Press Enter To View.");
}//GEN-LAST:event_txtQuestionNumberMouseEntered

private void btnPreviousMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPreviousMouseExited
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_btnPreviousMouseExited

private void btnNextMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNextMouseEntered
    //lblMsg.setText("Switch And View To Next Question.");
}//GEN-LAST:event_btnNextMouseEntered

private void btnNextMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNextMouseExited
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_btnNextMouseExited

private void txtQuestionNumberMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtQuestionNumberMouseExited
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_txtQuestionNumberMouseExited

private void btnLastMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLastMouseEntered
    //lblMsg.setText("Switch And View To Last Question.");
}//GEN-LAST:event_btnLastMouseEntered

private void btnLastMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLastMouseExited
    //lblMsg.setText("Welcome To Unit Test Wizard.");
}//GEN-LAST:event_btnLastMouseExited

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Object[] options = {"YES", "CANCEL"};
        int i = JOptionPane.showOptionDialog(null, "Are You Sure to Exit Test?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (i == 0) {
            timer.stop();
            new PracticeResultForm(unitTestBean, rollNo, true,classTestBean);
            this.dispose();
        }
    }//GEN-LAST:event_formWindowClosing

    private void questionPanel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_questionPanel1KeyPressed
        // TODO add your handling code here:
        
            
            
    }//GEN-LAST:event_questionPanel1KeyPressed

    private void questionPanel1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_questionPanel1KeyTyped
        // TODO add your handling code here:
       
    }//GEN-LAST:event_questionPanel1KeyTyped

    private void btnClearResponseItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_btnClearResponseItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_btnClearResponseItemStateChanged

    private void btnClearResponseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnClearResponseMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnClearResponseMouseEntered

    private void btnClearResponseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnClearResponseMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnClearResponseMouseExited

    private void btnClearResponseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearResponseActionPerformed
        // TODO add your handling code here:
            if(alQuestions.get(currentIndex).getType()==0||alQuestions.get(currentIndex).getType()==1)
            {
                  currentPanel.unlockSelection();
                JEE.unitTest.DBConnection d=new  JEE.unitTest.DBConnection();
                System.out.println(" ******"+unitTestBean.getUnitTestId()+"****** "+rollNo+" "+alQuestions.get(currentIndex).getQuestionId());
                d.clearResponseOption(unitTestBean.getUnitTestId(),rollNo, alQuestions.get(currentIndex)); 
               
                  alQuestions.get(currentIndex).setUserAnswer("UnAttempted");
               
            }
           else
           {
                
                JEE.unitTest.DBConnection d=new  JEE.unitTest.DBConnection();
                System.out.println(" ******"+unitTestBean.getUnitTestId()+"****** "+rollNo+" "+alQuestions.get(currentIndex).getQuestionId());
                d.clearResponseNumericals(unitTestBean.getUnitTestId(),rollNo, alQuestions.get(currentIndex));     
             
                currentPanel.clearTxt();
          }
        
             
    }//GEN-LAST:event_btnClearResponseActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(StartedUnitTestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(StartedUnitTestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(StartedUnitTestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(StartedUnitTestForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//                new StartedUnitTestForm().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel UnAttempted;
    private javax.swing.JButton btnClearResponse;
    private javax.swing.JButton btnEndPractice;
    private javax.swing.JButton btnFirst;
    private javax.swing.JButton btnLast;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnPrevious;
    private javax.swing.JButton btnSubmit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private ui.JPanelsSliding jPanelsSliding1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblSubjectName;
    private javax.swing.JLabel lblTimer;
    private javax.swing.JPanel pnlAllQue;
    private ui.QuestionPanel questionPanel1;
    private ui.QuestionPanel questionPanel2;
    private javax.swing.JRadioButton rdoDisable;
    private javax.swing.JRadioButton rdoEnable;
    private javax.swing.JTextField txtQuestionNumber;
    // End of variables declaration//GEN-END:variables

    private void SaveTestStatus() {
        int testId=unitTestBean.getUnitTestId();
        int totalQue=unitTestBean.getQuestions().size();
//        String userAnswer = con.getResult(unitTestBean.getUnitTestId(), questionBean.getQuestion_Id());
    }

    private void unlockSelection() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}