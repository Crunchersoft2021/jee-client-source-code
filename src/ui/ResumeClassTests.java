package ui;

import java.awt.Color;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import JEE.practice.*;
import JEE.test.TestBean;
import JEE.unitTest.UnitTestBean;
import com.bean.ClassTestBean;
import com.bean.StudentBean;
import java.text.SimpleDateFormat;
import javax.swing.ImageIcon;

public class ResumeClassTests extends javax.swing.JFrame {

    ArrayList<Integer> testId = new ArrayList<Integer>();
    ArrayList<Integer> totalQue = new ArrayList<Integer>();
    ArrayList<String> testInfo = new ArrayList<String>();
    ArrayList<TestBean> testBean1;
    ArrayList<PracticeBean> practiceBean1;
    ArrayList<UnitTestBean> unitTestBean1;
    ArrayList<Integer> testIds1;
    java.awt.event.MouseEvent evt111;
    int subjectId, chapterId, i = 0, testId1 = 0;
    Calendar cal = Calendar.getInstance();
    Calendar cal1 = Calendar.getInstance();
    boolean flag = true;
    int rollNo, subId;
    String initial = "";
    Connection con1;
    int testTime;
    static int testNextTime=0;
    ArrayList<ClassTestBean> classTestList = new ArrayList<ClassTestBean>();
    StudentBean studentBean=new StudentBean();
    ClassTestBean classTestBean=new ClassTestBean();
    /** Creates new form TestResultForm */
    public void tablesetting() {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        tblPractice.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tblPractice.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tblPractice.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tblPractice.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        tblPractice.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
        
    }

    private void clearTableRows(javax.swing.JTable tblPractice) {
        DefaultTableModel model = (DefaultTableModel) tblPractice.getModel();
        int rowCount = model.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    private void resumeTest() {
        JEE.practice.DBConnection con = new JEE.practice.DBConnection();
        testInfo = con.getClassTestInfo();
        classTestList=con.getClassTestInfo1();
       
        studentBean=con.getStudentInfo(rollNo);
        
                DefaultTableModel model = (DefaultTableModel) tblPractice.getModel();
                clearTableRows(tblPractice);
                for (int i = 0; i < testInfo.size(); i++) {
                    if(studentBean.getAcademicYear().equals( classTestList.get(i).getAcademicYear()) &&
                       studentBean.getStandard().equals( classTestList.get(i).getClass_Std()) &&  
                        studentBean.getDivision().equals( classTestList.get(i).getDivission()))
                    {
                            testTime = con.getTestTime();
                            System.out.println("testTime= **************"+testTime);
                            String temp = testInfo.get(i);

                            System.out.println("temp="+temp);
                            String[] obj = temp.split("\t");
                            int testId = Integer.parseInt(obj[0]);

                            System.out.println("testId"+testId);
                            this.testId1 = testId;         
                            int count = con.getQuestionCount(rollNo);
                          
                            String testName=con.getTestName(testId);
                            this.initial = con.getInitial(rollNo);
                            System.out.println("initial"+initial);
                            String date = obj[2];
                            String status = obj[1];
                          model.addRow(new Object[]{testId+"  ["+testName+"]", count, date, status, testTime + " Min."});
                    }  
                    else
                    {
                         model.addRow(new Object[]{" ", " ", " ", " ", " " });
                    }
                }   
        
    }
    
    public ResumeClassTests(int rollNo) {
        initComponents();
        this.rollNo = rollNo;
        System.out.println("rollno-------------->"+rollNo);
        String titl = new SetFrameTitle().setTitle();
        getContentPane().setBackground(Color.white);
        setTitle(titl);
        String lgo = new SetFrameTitle().setLogo();
        setIconImage(new ImageIcon(getClass().getResource(lgo)).getImage());
        tablesetting();
        resumeTest();
        setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        btnStart = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        panePhysics = new javax.swing.JScrollPane();
        tblPractice = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; //Disallow the editing of any cell
            }
        }
        ;

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("CruncherSoft's NEET+JEE Software 2014");
        setBackground(new java.awt.Color(196, 223, 254));
        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
            }
            public void windowLostFocus(java.awt.event.WindowEvent evt) {
                formWindowLostFocus(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(29, 9, 44));
        jPanel3.setName("jPanel3"); // NOI18N

        btnStart.setBackground(new java.awt.Color(255, 255, 255));
        btnStart.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        btnStart.setForeground(new java.awt.Color(29, 9, 44));
        btnStart.setText("Start Test");
        btnStart.setName("btnStart"); // NOI18N
        btnStart.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnStartMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnStartMouseExited(evt);
            }
        });
        btnStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStartActionPerformed(evt);
            }
        });

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setFont(new java.awt.Font("Segoe UI Semibold", 1, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(29, 9, 44));
        jButton1.setText("Home");
        jButton1.setName("jButton1"); // NOI18N
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton1MouseExited(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(177, Short.MAX_VALUE)
                .addComponent(btnStart)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnStart)
                    .addComponent(jButton1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(29, 9, 44));
        jPanel4.setName("jPanel4"); // NOI18N

        panePhysics.setBackground(new java.awt.Color(29, 9, 44));
        panePhysics.setFont(new java.awt.Font("Century", 1, 11)); // NOI18N
        panePhysics.setName("panePhysics"); // NOI18N

        ((DefaultTableCellRenderer)tblPractice.getTableHeader().getDefaultRenderer())
        .setHorizontalAlignment(JLabel.CENTER);
        tblPractice.setAutoCreateRowSorter(true);
        tblPractice.setBackground(new java.awt.Color(0, 0, 0));
        tblPractice.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        tblPractice.setForeground(new java.awt.Color(255, 255, 255));
        tblPractice.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Test Id", "Total Questions", "Date Time", "Status", "Test Time"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, true, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblPractice.setToolTipText("Double Click To View Detail Result.");
        tblPractice.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tblPractice.setGridColor(new java.awt.Color(0, 0, 0));
        tblPractice.setName("tblPractice"); // NOI18N
        tblPractice.setSelectionForeground(new java.awt.Color(204, 0, 0));
        tblPractice.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblPractice.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblPracticeMouseClicked(evt);
            }
        });
        panePhysics.setViewportView(tblPractice);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panePhysics, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 313, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panePhysics, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void tblPracticeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPracticeMouseClicked
    if (evt.getClickCount() == 2) {
        
        int row = tblPractice.getSelectedRow();
        row++;
        System.out.println("1***testId="+testId);
        if (row > 0) {
            JEE.test.DBConnection con = new JEE.test.DBConnection();
            classTestBean=con.getClassTestInfo1();
            int ava = con.checkStatus(rollNo);
            if (ava == 1) {
                JOptionPane.showMessageDialog(null, "You already attended this test.");
            } else {
                    
                    String CrrentDate= new SimpleDateFormat("MM/dd/yyyy").format(new java.sql.Timestamp(System.currentTimeMillis()));
//                    if(CrrentDate.equals(classTestBean.getEXPIRE_DATE())){   
//                        System.out.println("date==  "+classTestBean.getEXPIRE_DATE());
//                 
//                        JOptionPane.showMessageDialog(null, "Test Date Expire.");
//                    }
//                    else
//                    {
                            UnitTestBean unitTestBean = con.getClassTestBean1(testId1, initial);
                            unitTestBean.setUnitTestId(testId1);
                            if (unitTestBean.getStatus().equals("Available")) {
                                unitTestBean.setSubjectId(subId);
                              //
                             ///   testTime = con.getClassTestTime(testId1);
                                                                
                                System.out.println("testId1  ----------"+testId1);
                                System.out.println("prctice table testTime=="+testTime);
                                
                             
                                
                               // if (testTime == 0) {
                               //     unitTestBean.setTotalTime(60);
                               // } else {
                                    unitTestBean.setTotalTime(testTime);
                               // }
                                if (unitTestBean != null) {
                                    con.setStatus(rollNo);
                                    StartedUnitTestForm t = new StartedUnitTestForm(unitTestBean, true, rollNo, true,classTestBean);
                                    t.setVisible(true);
                                    this.dispose();
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "This Test Is Not Available Please Try Others.");
                            }
                   // }               
                            
            }
        }
    }
}//GEN-LAST:event_tblPracticeMouseClicked

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
//    new NewTestForm(rollNo).setVisible(true);
    this.dispose();
}//GEN-LAST:event_jButton1ActionPerformed

private void jButton1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseEntered
    jButton1.setForeground(Color.red);
}//GEN-LAST:event_jButton1MouseEntered

private void jButton1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseExited
    jButton1.setForeground(Color.black);
}//GEN-LAST:event_jButton1MouseExited

private void btnStartMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStartMouseEntered
// TODO add your handling code here:
}//GEN-LAST:event_btnStartMouseEntered

private void btnStartMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStartMouseExited
// TODO add your handling code here:
}//GEN-LAST:event_btnStartMouseExited

private void btnStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStartActionPerformed
    
    
    int row = tblPractice.getSelectedRow();
    row++;
    if (row > 0) {
        System.out.println("");
        JEE.test.DBConnection con = new JEE.test.DBConnection();
        classTestBean=con.getClassTestInfo1();
        int ava = con.checkStatus(rollNo);
        if (ava == 1) {
            JOptionPane.showMessageDialog(null, "You already attended this test.");
        } else {
             
              String CrrentDate= new SimpleDateFormat("MM/dd/yyyy").format(new java.sql.Timestamp(System.currentTimeMillis()));
//           if(CrrentDate.equals(classTestBean.getEXPIRE_DATE())){              
//               JOptionPane.showMessageDialog(null, "Test Date Expire.");
//           }
//           else
//           {
                    UnitTestBean unitTestBean = con.getClassTestBean1(testId1, initial);
                    unitTestBean.setUnitTestId(testId1);
                    if (unitTestBean.getStatus().equals("Available")) {
                        subId = con.getSubjectIdofTest(testId1);
                        unitTestBean.setSubjectId(subId);
                        //testTime = con.getClassTestTime(testId1);
                          System.out.println("testId1  ----------"+testId1);
                        System.out.println("start btn testTime=="+testTime);
                         testNextTime=testTime;
                                
                       // if (testTime == 0) {
                        //    unitTestBean.setTotalTime(60);
                        //} else {
                            unitTestBean.setTotalTime(testTime);
                       // }
                        if (unitTestBean != null) {
                            con.setStatus(rollNo);
                            StartedUnitTestForm t = new StartedUnitTestForm(unitTestBean, true, rollNo, true,classTestBean);
                            t.setVisible(true);
                            this.dispose();
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "This Test Is Not Available Please Try Others.");
                    }
                    
           //}    //
        }
    }
     else {
                        JOptionPane.showMessageDialog(null, "Please Select Test First");
     }
}//GEN-LAST:event_btnStartActionPerformed

    private void formWindowLostFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowLostFocus
//        this.dispose();
    }//GEN-LAST:event_formWindowLostFocus

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        new NewTestForm(rollNo).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(PracticeResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(PracticeResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(PracticeResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(PracticeResultForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
//                new PracticeResultForm().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnStart;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane panePhysics;
    private javax.swing.JTable tblPractice;
    // End of variables declaration//GEN-END:variables
}