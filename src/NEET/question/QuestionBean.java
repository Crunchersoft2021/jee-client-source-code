/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NEET.question;

import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class QuestionBean implements Serializable
{
    private String question,A,B,C,D,Answer,Hint,optionImagePath,hintImagePath,QuestionImagePath,userAnswer;
    private int sub_Id,Chap_Id,Topic_Id,Question_Id,Attempt;
    private boolean isQuestionAsImage,isOptionAsImage,isHintAsImage;

    public QuestionBean()
    {
        
    }
    
    public QuestionBean(int Question_Id,String question, String A, String B, String C, String D, String Answer, String Hint,  int sub_Id, int Chap_Id, int Topic_Id,  boolean isQuestionAsImage,  String QuestionImagePath,boolean isOptionAsImage,String optionImagePath,boolean isHintAsImage,String hintImagePath,int Attempt) {
        this.question = question;
        this.A = A;
        this.B = B;
        this.C = C;
        this.D = D;
        this.Answer = Answer;
        this.Hint = Hint;
        this.optionImagePath = optionImagePath;
        this.QuestionImagePath = QuestionImagePath;
        this.hintImagePath = hintImagePath;
        this.sub_Id = sub_Id;
        this.Chap_Id = Chap_Id;
        this.Topic_Id = Topic_Id;
        this.Question_Id = Question_Id;
        this.isQuestionAsImage = isQuestionAsImage;
        this.isHintAsImage = isHintAsImage;
        this.isOptionAsImage = isOptionAsImage;
        this.Attempt=Attempt;
        userAnswer="UnAttempted";
    }

    public void setHintImagePath(String hintImagePath) {
        this.hintImagePath = hintImagePath;
    }

    public void setIsHintAsImage(boolean isHintAsImage) {
        this.isHintAsImage = isHintAsImage;
    }

    public String getHintImagePath() {
        return hintImagePath;
    }

    public boolean isIsHintAsImage() {
        return isHintAsImage;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public int getAttempt() {
        return Attempt;
    }

    public void setAttempt(int Attempt) {
        this.Attempt = Attempt;
    }

    public String getA() {
        return A;
    }

    public void setA(String A) {
        this.A = A;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String Answer) {
        this.Answer = Answer;
    }

    public String getB() {
        return B;
    }

    public void setB(String B) {
        this.B = B;
    }

    public String getC() {
        return C;
    }

    public void setC(String C) {
        this.C = C;
    }

    public int getChap_Id() {
        return Chap_Id;
    }

    public void setChap_Id(int Chap_Id) {
        this.Chap_Id = Chap_Id;
    }

    public String getD() {
        return D;
    }

    public void setD(String D) {
        this.D = D;
    }

    public String getHint() {
        return Hint;
    }

    public void setHint(String Hint) {
        this.Hint = Hint;
    }

    public String getQuestionImagePath() {
        return QuestionImagePath;
    }

    public void setQuestionImagePath(String QuestionImagePath) {
        this.QuestionImagePath = QuestionImagePath;
    }

    public int getQuestion_Id() {
        return Question_Id;
    }

    public void setQuestion_Id(int Question_Id) {
        this.Question_Id = Question_Id;
    }

    public int getTopic_Id() {
        return Topic_Id;
    }

    public boolean isIsOptionAsImage() {
        return isOptionAsImage;
    }

    public void setIsOptionAsImage(boolean isOptionAsImage) {
        this.isOptionAsImage = isOptionAsImage;
    }

    public boolean isIsQuestionAsImage() {
        return isQuestionAsImage;
    }

    public void setIsQuestionAsImage(boolean isQuestionAsImage) {
        this.isQuestionAsImage = isQuestionAsImage;
    }

    public void setTopic_Id(int Topic_Id) {
        this.Topic_Id = Topic_Id;
    }

    public String getOptionImagePath() {
        return optionImagePath;
    }

    public void setOptionImagePath(String optionImagePath) {
        this.optionImagePath = optionImagePath;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getSub_Id() {
        return sub_Id;
    }

    public void setSub_Id(int sub_Id) {
        this.sub_Id = sub_Id;
    }
}