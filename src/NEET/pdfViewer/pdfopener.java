package NEET.pdfViewer;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.logging.*;

public class pdfopener {
    public void helpview(String pdfName) {
        File myFile = new File("allPdf/"+pdfName+".pdf");
        try {
            Desktop.getDesktop().open(myFile);
            
        } 
        catch (IOException ex) {
            Logger.getLogger(pdfopener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 }