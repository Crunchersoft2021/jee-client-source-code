/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.operations;




import com.bean.SubjectBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.DBConnection1;
import server.Server;

/**
 *
 * @author Aniket
 */
public class SubjectOperation {
    
    private Connection conn = null;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private Statement stmt=null;
    
     
    public SubjectBean getSubjectBean1(int subjectId) {
        SubjectBean subjectBean = null;
        try {
            conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            String query = "Select * from Subject_Info where Subject_Id=" +subjectId;
            ps=conn.prepareStatement(query);
//            ps.setInt(1, subjectId);
            rs=ps.executeQuery();
            
            while(rs.next()) {
                subjectBean = new SubjectBean();
                subjectBean.setSubjectId(rs.getInt(1));
                subjectBean.setSubjectName(rs.getString(2)); 
                subjectBean.setMarksPerQuestion(rs.getDouble(3));
                subjectBean.setMarksPerWrong(rs.getDouble(4));
                return subjectBean;
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return subjectBean;
    }
    public SubjectBean getSubjectBean(int subjectId) {
        SubjectBean subjectBean = null;
        try {
            conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            String query = "Select * from Subject_Info where Subject_Id=?";
            ps=conn.prepareStatement(query);
            ps.setInt(1, subjectId);
            rs=ps.executeQuery();
            
            while(rs.next()) {
                subjectBean = new SubjectBean();
                subjectBean.setSubjectId(rs.getInt(1));
                subjectBean.setSubjectName(rs.getString(2)); 
                subjectBean.setMarksPerQuestion(rs.getDouble(3));
                subjectBean.setMarksPerWrong(rs.getDouble(4));
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return subjectBean;
    }
    
    public ArrayList<SubjectBean> getSubjectsList() {
        ArrayList<SubjectBean> subjectList = new ArrayList<SubjectBean>();
        SubjectBean subjectBean = null;
        try {
            conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            String query = "SELECT * FROM SUBJECT_INFO WHERE ACTIVE_STATUS = ?";
            ps = conn.prepareStatement(query);
            ps.setBoolean(1, true);
            rs = ps.executeQuery();
            while(rs.next()) {
                subjectBean = new SubjectBean();
                subjectBean.setSubjectId(rs.getInt(1));
                subjectBean.setSubjectName(rs.getString(2));   
                subjectList.add(subjectBean);
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally {
            sqlClose();
        }
        return subjectList;
    }
    
    
    
    
    
    private void sqlClose() {
        try {
            if(rs != null)
                rs.close();
            if(ps != null)
                ps.close();
            if(conn != null)
                conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    public ArrayList<Double> getPMarks(ArrayList<Integer> subids) {
        ArrayList<Double> pmarks = new ArrayList<Double>();
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
             stmt = conn.createStatement();
            for (int i = 0; i <subids.size(); i++) {
                rs = stmt.executeQuery("Select distinct Marks_Per_Question from  Subject_Info where Subject_Id=" + subids.get(i));
                while (rs.next()) {
                    pmarks.add(rs.getDouble(1));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return pmarks;
    }

    public ArrayList<Double> getNMarks(ArrayList<Integer> subids) {
        ArrayList<Double> pmarks = new ArrayList<Double>();
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
       
        try {
            stmt= conn.createStatement();
            for (int i = 0; i < subids.size(); i++) {
                rs = stmt.executeQuery("Select distinct Marks_Per_Wrong_Question from  Subject_Info where Subject_Id=" + subids.get(i));
                while (rs.next()) {
                    pmarks.add(rs.getDouble(1));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return pmarks;
    }

    public ArrayList<String> getAllSubjectName(ArrayList<Integer> subids) {
        ArrayList<String> pmarks = new ArrayList<String>();
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt = conn.createStatement();
            for (int i = 0; i < subids.size(); i++) {
                rs = stmt.executeQuery("Select distinct Subject_Name from  Subject_Info where Subject_Id=" + subids.get(i));
                while (rs.next()) {
                    if (pmarks.contains(rs.getString(1))) {
                    } else {
                        pmarks.add(rs.getString(1));
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return pmarks;
    }

    public double getSPerwrongQuestionMarks(int subjectId) {
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
       
        try {
            
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select Marks_Per_Wrong_Question from Subject_Info where Subject_Id=" + subjectId);
            if (rs.next()) {
             
                 return rs.getDouble(4);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0.0;
    }

    public double getSPerQuestionMarks(int subjectId) {
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select Marks_Per_Question  from Subject_Info where Subject_Id=" + subjectId);
            if (rs.next()) {
                return rs.getDouble(1);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0.0;
    }
    
    public int getPerwrongQuestionMarks(int subjectId) {
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select Marks_Per_Wrong_Question from Subject_Info where Subject_Id=" + subjectId);
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }

    public int getPerQuestionMarks(int subjectId) {
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            
            stmt = conn.createStatement();
            rs = stmt.executeQuery("Select * from Subject_Info where Subject_Id=" + subjectId);
            if (rs.next()) {
                 
                 return rs.getInt(3);
            }
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }
    
    public String getSubject(int get) {
        String name = "";
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());

        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select Subject_Name from Subject_Info where Subject_Id=" + get);
            if (rs.next()) {
                name = rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return name;
    }
    
     public ArrayList<String> getSubject() {
        ArrayList<String> ret = new ArrayList<String>();
        int time = 0, count = 0, flag = 0;
        conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        
        try {
            stmt = conn.createStatement();
            count++;
            rs = stmt.executeQuery("Select Subject_Name from Subject_Info order by Subject_Id");
            while (rs.next()) {
                ret.add(rs.getString(1));
            }
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return ret;
    }
     
     public boolean UpdateSubjectMarks(SubjectBean subjectBean) {
        int SubjectId=subjectBean.getSubjectId();
        boolean returnValue = false;
        try {
            conn = new DBConnection1().getClientConnection1(Server.getServerIP());
            String query = "update Subject_Info set Marks_Per_Question = ? , Marks_Per_Wrong_Question = ? where Subject_Id=" +SubjectId;
            ps = conn.prepareStatement(query);           
            ps.setDouble(1,subjectBean.getMarksPerQuestion());
            ps.setDouble(2,subjectBean.getMarksPerWrong());           
            ps.executeUpdate();
            returnValue = true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            returnValue = false;
        } finally {
            sqlClose();
        }
        return returnValue;
    }
     
     public String getSubjectName(int SUBJECTID) {
        String SubjectName = null;
         conn = new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs1;
        try {
            Statement s = conn.createStatement();
            rs1 = s.executeQuery("Select * from SUBJECT_INFO where SUBJECT_ID=" + SUBJECTID);
            if (rs1.next()) {
                SubjectName = rs1.getString(2);
            }
            return SubjectName;
        } catch (SQLException ex) {
            Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(SubjectOperation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return SubjectName;
    }
}
