/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

import java.io.Serializable;

/**
 *
 * @author Aniket
 */
public class QuestionBean implements Serializable,Comparable<QuestionBean> {
    
    
    private String question;
    private String optionA;
    private String optionB;
    private String optionC;
    private String optionD;
    private String answer;
    private String hint;
    private String optionImagePath;
    private String hintImagePath;
    private String questionImagePath;
    private String year;
    private String userAnswer;
    private int questionId;
    private int subjectId;
    private int chapterId;
    private int topicId;
    private int attempt;
    private int level;
    private int type;
    private int View;   
    private boolean isQuestionAsImage;
    private boolean isOptionAsImage;
    private boolean isHintAsImage;
    private boolean selected;
    private String NumericalAnswer;
    private String Unit;

    

public QuestionBean()
    {
        
    }

    public QuestionBean(int questionId,String question, String optionA, String optionB, String optionC, String optionD, String answer, String hint,int level,int subjectId, int chapterId, int topicId,boolean isQuestionAsImage,String questionImagePath,boolean isOptionAsImage, String optionImagePath,boolean isHintAsImage, String hintImagePath, int attempt,  int type, String year,int View,String NumericalAnswer, String Unit) {
        this.question = question;
        this.optionA = optionA;
        this.optionB = optionB;
        this.optionC = optionC;
        this.optionD = optionD;
        this.answer = answer;
        this.hint = hint;
        this.optionImagePath = optionImagePath;
        this.hintImagePath = hintImagePath;
        this.questionImagePath = questionImagePath;
        this.year = year;
        
        this.questionId = questionId;
        this.subjectId = subjectId;
        this.chapterId = chapterId;
        this.topicId = topicId;
        this.attempt = attempt;
        this.level = level;
        this.type = type;
        this.View = View;
        this.isQuestionAsImage = isQuestionAsImage;
        this.isOptionAsImage = isOptionAsImage;
        this.isHintAsImage = isHintAsImage;
        this.userAnswer = "UnAttempted";
        this.NumericalAnswer=NumericalAnswer;
        this.Unit=Unit;
        
    }
 public String getNumericalAnswer() {
        return NumericalAnswer;
    }

    public void setNumericalAnswer(String NumericalAnswer) {
        this.NumericalAnswer = NumericalAnswer;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String Unit) {
        this.Unit = Unit;
    }
    public int getView() {
        return View;
    }

    public void setView(int View) {
        this.View = View;
    }
    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }
    
    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setHintImagePath(String hintImagePath) {
        this.hintImagePath = hintImagePath;
    }

    public void setIsHintAsImage(boolean isHintAsImage) {
        this.isHintAsImage = isHintAsImage;
    }

    public String getHintImagePath() {
        return hintImagePath;
    }

    public boolean isIsHintAsImage() {
        return isHintAsImage;
    }

    public int getAttempt() {
        return attempt;
    }

    public void setAttempt(int attempt) {
        this.attempt = attempt;
    }

    public String getOptionA() {
        return optionA;
    }

    public void setOptionA(String optionA) {
        this.optionA = optionA;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getOptionB() {
        return optionB;
    }

    public void setOptionB(String optionB) {
        this.optionB = optionB;
    }

    public String getOptionC() {
        return optionC;
    }

    public void setOptionC(String optionC) {
        this.optionC = optionC;
    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    public String getOptionD() {
        return optionD;
    }

    public void setOptionD(String optionD) {
        this.optionD = optionD;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getQuestionImagePath() {
        return questionImagePath;
    }

    public void setQuestionImagePath(String questionImagePath) {
        this.questionImagePath = questionImagePath;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getTopicId() {
        return topicId;
    }

    public boolean isIsOptionAsImage() {
        return isOptionAsImage;
    }

    public void setIsOptionAsImage(boolean isOptionAsImage) {
        this.isOptionAsImage = isOptionAsImage;
    }

    public boolean isIsQuestionAsImage() {
        return isQuestionAsImage;
    }

    public void setIsQuestionAsImage(boolean isQuestionAsImage) {
        this.isQuestionAsImage = isQuestionAsImage;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public String getOptionImagePath() {
        return optionImagePath;
    }

    public void setOptionImagePath(String optionImagePath) {
        this.optionImagePath = optionImagePath;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }
    
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public int compareTo(QuestionBean qb) {
        if(subjectId == qb.subjectId)
            return 0;
        else if(subjectId > qb.subjectId)
            return 1;
        else
            return -1;
    }
}