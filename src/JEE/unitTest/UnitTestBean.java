/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package JEE.unitTest;

import com.bean.QuestionBean;
import java.util.ArrayList;
import java.util.Date;

import java.io.Serializable;

/**
 *
 * @author 007
 */
public class UnitTestBean implements Serializable {
    private int unitTestId,subjectId;
    private ArrayList<Integer> chapterId;
    private Date startDateTime;
    private ArrayList<QuestionBean> questions;
    private String status;
    private long remainingTime;
    private int totalTime;    
    private int currentQuestionNumber;
    private boolean timeEnabled;

    public ArrayList<Integer> getChapterId() {
        return chapterId;
    }

    public int getCurrentQuestionNumber() {
        return currentQuestionNumber;
    }
//
//    public String getHint() {
//        return hint;
//    }

    public ArrayList<QuestionBean> getQuestions() {
        return questions;
    }

    public long getRemainingTime() {
        return remainingTime;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public String getStatus() {
        return status;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public boolean isTimeEnabled() {
        return timeEnabled;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public int getUnitTestId() {
        return unitTestId;
    }

    public void setChapterId(ArrayList<Integer> chapterId) {
        this.chapterId = chapterId;
    }

    public void setCurrentQuestionNumber(int currentQuestionNumber) {
        this.currentQuestionNumber = currentQuestionNumber;
    }
//
//    public void setHint(String hint) {
//        this.hint = hint;
//    }

    public void setQuestions(ArrayList<QuestionBean> questions) {
        this.questions = questions;
    }

    public void setRemainingTime(long remainingTime) {
        this.remainingTime = remainingTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public void setTimeEnabled(boolean timeEnabled) {
        this.timeEnabled = timeEnabled;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public void setUnitTestId(int unitTestId) {
        this.unitTestId = unitTestId;
    }
}