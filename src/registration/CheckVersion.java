package registration;

import java.sql.Timestamp;
import java.util.Date;
import javax.swing.JOptionPane;

public class CheckVersion {
    
   public static void checkCurrentDate()
   {
       Timestamp ts=new Timestamp(System.currentTimeMillis());
       Date d=new Date(113,5,30);
       Long l=d.getTime();
       if(ts.after(new Timestamp(l)))
       {
           JOptionPane.showMessageDialog(null,"This Version of Application has expired plz purchase a new one","Error",JOptionPane.ERROR_MESSAGE);
           System.exit(0);
       }
   }
   public static  void main(String s[])
   {
       CheckVersion.checkCurrentDate();
   }
}