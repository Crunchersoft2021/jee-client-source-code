/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package registration;

import server.DBConnection1;
import server.Server;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author 007
 */
public class UserLog {

    int roll;
    public UserLog(int rollNo)
    {
        this.roll=rollNo;
    }
    
    public void insertIntoLog()
    {
        
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        try {
            Statement st=conn.createStatement();
            st.execute("insert into LogInfo values('"+new Timestamp(System.currentTimeMillis())+"')");
        } catch (SQLException ex) {
            Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    public Timestamp displayLog()
    {
        Connection conn=new DBConnection1().getClientConnection1(Server.getServerIP());
        ResultSet rs;
        Timestamp t1=null;
        try {
            Statement st=conn.createStatement();
            rs=st.executeQuery("select * from LogInfo");
            while(rs.next())
            {
                t1=rs.getTimestamp(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CreateTableRegistration.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return t1;
    }
     public int compareDate()
        {
            int flag=0;
            if(new Timestamp(System.currentTimeMillis()).before(displayLog()))
            {
                JOptionPane.showMessageDialog(null,"Your System Date Is Not Correct.Application Will Close");
            }
            else
            {
                 flag=1;
            }
            //System.out.println(flag);
            return flag;
            
        }
     public int checkSubscription()
     {
         int flag=0;
         double z=31536000000.0;
         if(System.currentTimeMillis()>=(new DisplayRegistrationTime(roll).displayRegistration()+z))
         {
             JOptionPane.showMessageDialog(null,"This version has expired plz purchase new version");
             flag=1;
         }
         return flag;
     }
    
//    public static void main(String args[]) 
//    {
//       // s.execute("insert into LogInfo values('"+start_DateTime+"')");
////        new UserLog().insertIntoLog();
////        new UserLog().displayLog();
////        System.out.print(
////       new UserLog().checkSubscription());
//        new UserLog().resetlog();
//    }
}